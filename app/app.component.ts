import { Component, ViewChild, OnInit, OnDestroy, AfterViewInit, ChangeDetectorRef } from "@angular/core";
import { Page } from "ui/page";
import { ActionItem } from "ui/action-bar";
import { Observable } from "data/observable";
import { RadSideDrawerComponent, SideDrawerType } from "nativescript-ui-sidedrawer/angular";
import { RadSideDrawer,PushTransition,DrawerTransitionBase } from 'nativescript-ui-sidedrawer';
import {RouterExtensions} from "nativescript-angular/router";
import { StoreService } from "./shared/service/StoreService";
import { HttpPostService } from "./shared/service/HttpServicePost";
import { CommonUtils } from "./utils/CommonUtils";
import { AppConstants } from "./shared/AppConstants";


@Component({
  moduleId: module.id,
  templateUrl: "./app.html",
  styleUrls: ['./app.css']
})
export class AppComponent  implements AfterViewInit, OnInit, OnDestroy {
  private _sideDrawerTransition: DrawerTransitionBase;

  //ng-models
  userName = "";
  userType = "";
  isCustomer:boolean = false;
  isProvider:boolean = false;
  isLoggedIn:boolean;
  isLoading:boolean = false;

  constructor(private _changeDetectionRef: ChangeDetectorRef,
    private routerExtensions: RouterExtensions,
    private storeService: StoreService,
    private post: HttpPostService,
    private utils: CommonUtils) {
  }

  

  @ViewChild(RadSideDrawerComponent) public drawerComponent: RadSideDrawerComponent;
  private drawer: RadSideDrawer;
  ngAfterViewInit() {
      this.drawer = this.drawerComponent.sideDrawer;
      this._changeDetectionRef.detectChanges();
      this.sideDrawerTransition = new PushTransition();
  }

  ngOnInit() {
    this.isLoading = true;
    console.log("ngOnInit");
    this.storeService.categoryList = "[]";
    this.storeService.countryItems = "[]";
    //Load store values
    // let aCat = JSON.stringify(this.storeService.categoryList);
    // if(aCat.length == 0){
      this.post.getCategoryList("", msg => {
        this.storeService.categoryList = JSON.stringify(msg);
        //Load Home Page  -- default to /home
        if(!this.storeService.isLoggedIn)
          this.routerExtensions.navigate(['/login']);
        else if(this.storeService.userType == AppConstants.PROVIDER)
          this.routerExtensions.navigate(['/jobAlerts']);
        else
          this.routerExtensions.navigate(['/home']);
        this.refreshData();
        this.isLoading = false;
      });
    // }
    
  }

  ngOnDestroy(){}

  get sideDrawerTransition(): DrawerTransitionBase {
    return this._sideDrawerTransition;
  }
  set sideDrawerTransition(value: DrawerTransitionBase) {
    this._sideDrawerTransition = value;
  }

  public toggleDrawer() {
    this.refreshData();
    this.drawer.toggleDrawerState();
  }

  public btnSubmitQuote(){
    this.routerExtensions.navigate(["submitQuote"], { clearHistory: true });
    this.drawer.closeDrawer();
  }

  public btnJobAlerts(){
    this.routerExtensions.navigate(["jobAlerts"], { clearHistory: false });
    this.drawer.closeDrawer();
  }

  public btnMyJobs(){
    this.routerExtensions.navigate(["myJobs"], { clearHistory: false });
    this.drawer.closeDrawer();
  }

  public btnHome(){
    this.routerExtensions.navigate(["home"], { clearHistory: true });
    this.drawer.closeDrawer();
  }

  public btnLogin(){
     if(!this.storeService.isLoggedIn())
      this.routerExtensions.navigate(["login"], { clearHistory: true });
    this.drawer.closeDrawer();
  }

  public logoff() {
    this.storeService.cookie = "";
    this.storeService.nonce = "";
    this.storeService.userName = "";
    this.storeService.userPass = "";
    this.storeService.userType = "";
    this.storeService.userId = "";
    this.storeService.categoryList = "";
    this.refreshData();
    this.btnLogin();
  }

  public refreshData(){
    let loadStack:number = 1;
    this.userName = this.storeService.isLoggedIn()?this.storeService.userName:"Login";
    this.userType = this.storeService.isLoggedIn()?this.storeService.userType:"";
    this.isLoggedIn = this.storeService.isLoggedIn();
    this.isCustomer = (this.storeService.userType == AppConstants.CUSTOMER);
    this.isProvider = (this.storeService.userType == AppConstants.PROVIDER);
  }

}