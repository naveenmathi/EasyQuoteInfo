import { NgModule } from "@angular/core";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { DropDownModule } from "nativescript-drop-down/angular";
import { AppComponent } from "./app.component";
import { routes, navigatableComponents } from "./app.routing";
import { HttpPostService } from "./shared/service/HttpServicePost";
import { IfAndroidDirective, IfIosDirective } from "./if-platform.directive";
import { StoreService } from "./shared/service/StoreService";
import { CommonUtils } from "./utils/CommonUtils";

@NgModule({
  imports: [
    NativeScriptModule,
    NativeScriptFormsModule,
    NativeScriptHttpModule,
    NativeScriptRouterModule,
    NativeScriptRouterModule.forRoot(routes),
    NativeScriptHttpClientModule,
    NativeScriptUISideDrawerModule,
    DropDownModule,
  ],
  declarations: [
    AppComponent,
    ...navigatableComponents,
    IfAndroidDirective,
    IfIosDirective
  ],
  providers: [
    HttpPostService,
    StoreService,
    CommonUtils
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
 
}