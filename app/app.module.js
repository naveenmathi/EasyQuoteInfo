"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("nativescript-angular/forms");
var http_1 = require("nativescript-angular/http");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var router_1 = require("nativescript-angular/router");
var http_client_1 = require("nativescript-angular/http-client");
var angular_1 = require("nativescript-ui-sidedrawer/angular");
var angular_2 = require("nativescript-drop-down/angular");
var app_component_1 = require("./app.component");
var app_routing_1 = require("./app.routing");
var HttpServicePost_1 = require("./shared/service/HttpServicePost");
var if_platform_directive_1 = require("./if-platform.directive");
var StoreService_1 = require("./shared/service/StoreService");
var CommonUtils_1 = require("./utils/CommonUtils");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                nativescript_module_1.NativeScriptModule,
                forms_1.NativeScriptFormsModule,
                http_1.NativeScriptHttpModule,
                router_1.NativeScriptRouterModule,
                router_1.NativeScriptRouterModule.forRoot(app_routing_1.routes),
                http_client_1.NativeScriptHttpClientModule,
                angular_1.NativeScriptUISideDrawerModule,
                angular_2.DropDownModule,
            ],
            declarations: [
                app_component_1.AppComponent
            ].concat(app_routing_1.navigatableComponents, [
                if_platform_directive_1.IfAndroidDirective,
                if_platform_directive_1.IfIosDirective
            ]),
            providers: [
                HttpServicePost_1.HttpPostService,
                StoreService_1.StoreService,
                CommonUtils_1.CommonUtils
            ],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
