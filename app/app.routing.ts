import { LoginComponent } from "./pages/login/login.component";
import { SubmitQuoteComponent } from "./pages/submitQuote/submitQuote.component";
import { HomeComponent } from "./pages/home/home.component";
import { SignupComponent } from "./pages/signup/signup.component";
import { JobAlertsComponent } from "./pages/jobAlerts/jobAlerts.component";
import { MyJobsComponent } from "./pages/myJobs/myJobs.component";
import { SubmitVendorQComponent } from "./pages/submitVendorQ/submitVendorQ.component";
import { ListQuotesComponent } from "./pages/listQuotes/listQuotes.component";

export const routes = [
  { path: "home", component: HomeComponent },  
  { path: "login", component: LoginComponent },  
  { path: "signup", component: SignupComponent },  
  { path: "submitQuote", component: SubmitQuoteComponent },
  { path: "jobAlerts", component: JobAlertsComponent },
  { path: "myJobs", component: MyJobsComponent },
  { path: "submitVendorQ", component: SubmitVendorQComponent },
  { path: "listQuotes", component: ListQuotesComponent },
  
];

var componentBuilder = [];
for(let route of routes){
  componentBuilder.push(route.component);
}

export const navigatableComponents = componentBuilder;