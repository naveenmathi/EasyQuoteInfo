import { getString, setString } from "application-settings";
import { Injectable } from "@angular/core";

const tokenKey = "token";

@Injectable()
export class StoreService {

  constructor(){
  }

  public isLoggedIn() {
    return !!getString("cookie");
  }

  public get cookie(): string {
    return getString("cookie");
  }

  public set cookie(cookie: string) {
    setString("cookie", cookie);
  }

  public get userType(): string{
      return getString("userType");
  }

  public set userType(userType: string) {
    setString("userType", userType);
  }

  public get userName(): string{
    return getString("userName");
  }

  public set userName(userName: string) {
    setString("userName", userName);
  }

  public set categoryList(categoryList: string){
    setString("categoryList", categoryList);
  }

  public get categoryList(){
    return getString("categoryList");
  }

  public set countryItems(countryItems: string){
    setString("countryItems", countryItems);
  }

  public get countryItems(){
    return getString("countryItems");
  }

  public set userPass(userPass: string){
    setString("userPass", userPass);
  }

  public get userPass(){
    return getString("userPass");
  }

  public set nonce(nonce: string){
    setString("nonce", nonce);
  }

  public get nonce(){
    return getString("nonce");
  } 
  
  public set userCat(userCat: string){
    setString("userCat", userCat);
  }

  public get userCat(){
    return getString("userCat");
  }

  public set userCatTxt(userCatTxt: string){
    setString("userCatTxt", userCatTxt);
  }

  public get userCatTxt(){
    return getString("userCatTxt");
  }

  public set userId(userId:string){
    setString("userId", ""+userId);
  }

  public get userId(){
    return getString("userId");
  }

  public set userMail(userMail:string){
    setString("userMail", ""+userMail);
  }

  public get userMail(){
    return getString("userMail");
  }

  public set userPh(userPh:string){
    setString("userPh", ""+userPh);
  }

  public get userPh(){
    return getString("userPh");
  }

  public set firstName(firstName:string){
    setString("firstName", ""+firstName);
  }

  public get firstName(){
    return getString("firstName");
  }

  public set lastName(lastName:string){
    setString("lastName", ""+lastName);
  }

  public get lastName(){
    return getString("lastName");
  }

  public set podsOptions(podsOptions:string){
    setString("podsOptions", ""+podsOptions);
  }

  public get podsOptions(){
    return getString("podsOptions");
  }

  
}
