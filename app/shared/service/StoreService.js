"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var application_settings_1 = require("application-settings");
var core_1 = require("@angular/core");
var tokenKey = "token";
var StoreService = /** @class */ (function () {
    function StoreService() {
    }
    StoreService.prototype.isLoggedIn = function () {
        return !!application_settings_1.getString("cookie");
    };
    Object.defineProperty(StoreService.prototype, "cookie", {
        get: function () {
            return application_settings_1.getString("cookie");
        },
        set: function (cookie) {
            application_settings_1.setString("cookie", cookie);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StoreService.prototype, "userType", {
        get: function () {
            return application_settings_1.getString("userType");
        },
        set: function (userType) {
            application_settings_1.setString("userType", userType);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StoreService.prototype, "userName", {
        get: function () {
            return application_settings_1.getString("userName");
        },
        set: function (userName) {
            application_settings_1.setString("userName", userName);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StoreService.prototype, "categoryList", {
        get: function () {
            return application_settings_1.getString("categoryList");
        },
        set: function (categoryList) {
            application_settings_1.setString("categoryList", categoryList);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StoreService.prototype, "countryItems", {
        get: function () {
            return application_settings_1.getString("countryItems");
        },
        set: function (countryItems) {
            application_settings_1.setString("countryItems", countryItems);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StoreService.prototype, "userPass", {
        get: function () {
            return application_settings_1.getString("userPass");
        },
        set: function (userPass) {
            application_settings_1.setString("userPass", userPass);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StoreService.prototype, "nonce", {
        get: function () {
            return application_settings_1.getString("nonce");
        },
        set: function (nonce) {
            application_settings_1.setString("nonce", nonce);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StoreService.prototype, "userCat", {
        get: function () {
            return application_settings_1.getString("userCat");
        },
        set: function (userCat) {
            application_settings_1.setString("userCat", userCat);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StoreService.prototype, "userCatTxt", {
        get: function () {
            return application_settings_1.getString("userCatTxt");
        },
        set: function (userCatTxt) {
            application_settings_1.setString("userCatTxt", userCatTxt);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StoreService.prototype, "userId", {
        get: function () {
            return application_settings_1.getString("userId");
        },
        set: function (userId) {
            application_settings_1.setString("userId", "" + userId);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StoreService.prototype, "userMail", {
        get: function () {
            return application_settings_1.getString("userMail");
        },
        set: function (userMail) {
            application_settings_1.setString("userMail", "" + userMail);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StoreService.prototype, "userPh", {
        get: function () {
            return application_settings_1.getString("userPh");
        },
        set: function (userPh) {
            application_settings_1.setString("userPh", "" + userPh);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StoreService.prototype, "firstName", {
        get: function () {
            return application_settings_1.getString("firstName");
        },
        set: function (firstName) {
            application_settings_1.setString("firstName", "" + firstName);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StoreService.prototype, "lastName", {
        get: function () {
            return application_settings_1.getString("lastName");
        },
        set: function (lastName) {
            application_settings_1.setString("lastName", "" + lastName);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StoreService.prototype, "podsOptions", {
        get: function () {
            return application_settings_1.getString("podsOptions");
        },
        set: function (podsOptions) {
            application_settings_1.setString("podsOptions", "" + podsOptions);
        },
        enumerable: true,
        configurable: true
    });
    StoreService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], StoreService);
    return StoreService;
}());
exports.StoreService = StoreService;
