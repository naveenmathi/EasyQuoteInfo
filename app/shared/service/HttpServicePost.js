"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/map");
require("rxjs/add/operator/catch");
require("rxjs/add/observable/throw");
var http_1 = require("@angular/common/http");
var AppConstants_1 = require("../../shared/AppConstants");
var bghttp = require("nativescript-background-http");
var HttpPostService = /** @class */ (function () {
    function HttpPostService(http) {
        this.http = http;
        this.mirrorServerURL = "https://httpbin.org/post";
        // private baseUrl:string = AppConstants.BASE_URL;
        this.baseUrl = "http://www.cingotest.com/easy_quote_info/wp-json/wp/v2/";
        this.apiUrl = "http://cingotest.com/easy_quote_info/api/";
        this.apiFUrl = "http://cingotest.com/easy_quote_info/apiF/get.php?funcName=";
        this.apiFPostUrl = "http://cingotest.com/easy_quote_info/apiF/post.php?funcName=";
    }
    HttpPostService.prototype.postData = function (url, data) {
        var options = this.createRequestOptions();
        return this.http.post(url, { data: data }, { headers: options })
            .map(function (res) { return res; })
            .catch(this.handleErrors);
    };
    HttpPostService.prototype.postFormData = function (url, data) {
        // url="http://192.168.1.5:81/test.php";
        var formData = new FormData();
        Object.keys(data.formData).map(function (key) {
            formData.append(key, data.formData[key]);
        });
        var headers = new http_1.HttpHeaders({ 'enctype': 'multipart/form-data' });
        return this.http.post(url, formData, { headers: headers })
            .map(function (res) { return res; })
            .catch(this.handleErrors);
    };
    HttpPostService.prototype.postDataFull = function (url, data) {
        var options = this.createRequestOptions();
        return this.http.post(url, { data: data }, { headers: options })
            .map(function (res) { return res; })
            .catch(this.handleErrorsFull);
    };
    HttpPostService.prototype.fileUpload = function (url, data) {
        // "/storage/emulated/0/Pictures/flower.jpeg", "flower", 'http://192.168.1.4:81/test.php'
        return new Promise(function (resolve, reject) {
            var s = bghttp.session("file-upload");
            var req = {
                url: url,
                method: "POST",
                headers: {
                    "Content-Type": "application/octet-stream",
                    "File-Name": data.jobFile.replace(/^.*[\\\/]/, '')
                },
                description: 'uploading job file'
            };
            var params = [];
            if (data.formData) {
                Object.keys(data.formData).map(function (key) {
                    params.push({ name: "" + key, value: "" + data.formData[key] });
                    console.log(key + " " + data.formData[key]);
                });
            }
            params.push({ name: "fileToUpload", filename: data.jobFile, mimeType: 'application/pdf' });
            var task = s.multipartUpload(params, req);
            var responseData = "";
            task.on("error", function (err) {
                console.log("Error " + JSON.stringify(err));
                reject(Error("Error uploading file"));
            });
            task.on("complete", function (res) {
                console.log("Complete " + JSON.stringify(res));
                resolve(responseData);
            });
            task.on("responded", function (e) {
                responseData = responseData + e.data;
                console.log("Responded " + JSON.stringify(e));
            });
        });
    };
    // postMultipartData(url:string, data:any, callBack:Function){
    //     // .subscribe(
    //     //     res => {
    //     //         callBack((<any>res).json.data);
    //     //     },
    //     //     () => {
    //     //         callBack ("Error while posting Job!");
    //     //     }
    //     // );
    //     var form_data = new FormData();
    //     for ( var key in data ) {
    //         form_data.append(key, data[key]);
    //     }
    //     var session = bghttp.session("image-upload");
    //     var request = {
    //         url:url,
    //         method: "POST",
    //         headers: {
    //             "Content-Type": "application/octet-stream",
    //             "File-Name": 'gib'
    //         },
    //         // description: description
    //     };
    //     // if (should_fail) {
    //     //     request.headers["Should-Fail"] = true;
    //     // }
    //     let task: bghttp.Task;
    //     var params = [
    //                 { name: "test", value: "value" },
    //                 { name: "fileToUpload", filename: '/storage/emulated/0/Pictures/flower.jpeg', mimeType: 'image/jpeg' }
    //             ];
    //     task = session.multipartUpload(params, request);
    //     task.on("progress", logEvent);
    //     task.on("error", logEvent);
    //     task.on("responded", logEvent);
    //     function logEvent(e) {
    //         // console.log("currentBytes: " + e.currentBytes);
    //         console.log("totalBytes: " + e.totalBytes);
    //         // console.log("eventName: " + e.eventName);
    //         console.log("Response: "+JSON.stringify(e));
    //     }
    // }
    HttpPostService.prototype.getData = function (url) {
        return this.http.get(url)
            .map(function (res) { return res; })
            .catch(this.handleErrors);
    };
    HttpPostService.prototype.getDataFull = function (url) {
        return this.http.get(url)
            .map(function (res) { return res; })
            .catch(this.handleErrorsFull);
    };
    HttpPostService.prototype.createRequestOptions = function () {
        var headers = new http_1.HttpHeaders();
        headers.set("AuthKey", "my-key");
        headers.set("AuthToken", "my-token");
        headers.set("Content-Type", "application/json");
        headers.set("Cache-Control", "no-cache");
        return headers;
    };
    HttpPostService.prototype.handleErrors = function (error) {
        console.log(JSON.stringify(error));
        return Observable_1.Observable.throw(error);
    };
    HttpPostService.prototype.handleErrorsFull = function (error) {
        console.log(JSON.stringify(error));
        if (error.status == '0')
            alert("Please connect to Internet!");
        else
            return Observable_1.Observable.throw(error);
    };
    //API CALLS
    HttpPostService.prototype.getCountryItems = function (data, callBack) {
        return this.getData(this.apiFUrl + "service_finder_get_countries").subscribe(function (res) {
            callBack(res);
        }, function (e) {
            console.log(JSON.stringify(e));
        });
    };
    HttpPostService.prototype.getCategoryList = function (data, callBack) {
        return this.getData(this.baseUrl + "providers-category?per_page=100").subscribe(function (res) {
            //console.log(JSON.stringify(res[1]))
            callBack(res);
        }, function (e) {
            console.log(e);
        });
    };
    HttpPostService.prototype.getCoutries = function (data, callBack) {
        return this.getData(this.apiFUrl + "getCountries").subscribe(function (res) {
            callBack(res);
        }, function (e) {
            console.log(e);
        });
    };
    HttpPostService.prototype.getPodsOptions = function (data, callBack) {
        return this.getData(this.apiFUrl + "getPodsOptions").subscribe(function (res) {
            callBack(res);
        }, function (e) {
            console.log(e);
        });
    };
    HttpPostService.prototype.postJob = function (data, callBack) {
        var url = this.apiFPostUrl + 'submitJob';
        if (data.jobFile) {
            this.fileUpload(url, data)
                .then(function (res) {
                callBack(res);
            })
                .catch(function (err) {
                callBack(err);
            });
        }
        else {
            this.postFormData(url, data)
                .subscribe(function (res) {
                callBack(res);
            }, function () {
                callBack("Error while submitting Job!");
            });
        }
    };
    HttpPostService.prototype.postQuote = function (data, callBack) {
        var url = this.apiFPostUrl + 'submitVendorQ';
        if (data.jobFile) {
            this.fileUpload(url, data)
                .then(function (res) {
                callBack(res);
            })
                .catch(function (err) {
                callBack(err);
            });
        }
    };
    HttpPostService.prototype.loginUser = function (data, callBack) {
        var _this = this;
        var urlA = this.apiUrl + "get_nonce/?controller=auth&method=generate_auth_cookie";
        this.getDataFull(urlA).subscribe(function (res) {
            // alert(JSON.stringify(res));
            var nonce = res.nonce;
            if (nonce) {
                urlA = _this.apiUrl + "auth/generate_auth_cookie/?nonce="
                    + nonce
                    + "&username="
                    + data.username
                    + "&password="
                    + data.password
                    + "&insecure=cool";
                _this.getDataFull(urlA).subscribe(function (res2) {
                    // alert(JSON.stringify(res2));
                    var cookie = res2.cookie;
                    if (cookie) {
                        var res2SavePoint_1 = res2;
                        urlA = _this.apiUrl + "user/get_user_meta/?cookie="
                            + cookie
                            + "&insecure=cool";
                        _this.getDataFull(urlA).subscribe(function (res3) {
                            // alert(JSON.stringify(res3));
                            var userType = "";
                            var userCat = "";
                            if (res3.status == "ok") {
                                var capab = res3.es_capabilities;
                                // alert(capab);
                                if (capab.includes("Customer")) {
                                    userType = AppConstants_1.AppConstants.CUSTOMER;
                                }
                                else if (capab.includes("Provider")) {
                                    userType = AppConstants_1.AppConstants.PROVIDER;
                                    userCat = res3.primary_category;
                                }
                                callBack({ cookie: cookie, nonce: nonce, userType: userType,
                                    userCat: userCat, userId: res2SavePoint_1.user.id,
                                    userEmail: res2SavePoint_1.user.email,
                                    userFirstName: res3.first_name,
                                    usreLastName: res3.last_name,
                                    userName: res3.first_name + " " + res3.last_name });
                            }
                        }, function (e) {
                            callBack(e);
                        });
                    }
                    else {
                        callBack(res2);
                    }
                }, function (e) {
                    callBack(e);
                });
            }
        }, function (e) {
            callBack(e);
        });
    };
    HttpPostService.prototype.getJobListings = function (data, callBack) {
        var aURL = this.apiFUrl + "getJobListing&jobId=" + data.catId;
        return this.getDataFull(aURL).subscribe(function (res) {
            callBack(res);
        }, function () {
            alert("Error while fetching job listings");
        });
    };
    HttpPostService.prototype.getPostsByAuthor = function (data, callBack) {
        var aURL = this.apiFUrl + "getPostsByAuthor&USER_ID=" + data.USER_ID;
        return this.getDataFull(aURL).subscribe(function (res) {
            callBack(res);
        }, function () {
            alert("Error while job posts");
        });
    };
    HttpPostService.prototype.getQuotesForJob = function (data, callBack) {
        var aURL = this.apiFUrl + "getQuotesForJob&JOB_ID=" + data.JOB_ID;
        return this.getDataFull(aURL).subscribe(function (res) {
            callBack(res);
        }, function () {
            alert("Error while job posts");
        });
    };
    HttpPostService.prototype.getListingAuthor = function (data, callBack) {
        var aURL = this.apiFUrl + "getListingAuthor&postId=" + data.postId;
        return this.getDataFull(aURL).subscribe(function (res) {
            callBack(res);
        }, function () {
            alert("Error while fetching job listings");
        });
    };
    HttpPostService.prototype.findProvider = function (data, callBack) {
        var aURL = this.apiFUrl + "findProvider"
            + "&categoryId=" + data.categoryId
            + "&searchString=" + data.searchString
            + "&country=" + data.country
            + "&city=" + data.city;
        console.log(aURL);
        return this.getDataFull(aURL).subscribe(function (res) {
            callBack(res);
        }, function () {
            alert("Error while searching!");
        });
    };
    HttpPostService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], HttpPostService);
    return HttpPostService;
}());
exports.HttpPostService = HttpPostService;
