import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { AppConstants } from "../../shared/AppConstants";
import * as bghttp from "nativescript-background-http";

@Injectable()
export class HttpPostService {
    private mirrorServerURL:string = "https://httpbin.org/post";
    // private baseUrl:string = AppConstants.BASE_URL;
    private baseUrl:string = "http://www.cingotest.com/easy_quote_info/wp-json/wp/v2/";
    private apiUrl:string = "http://cingotest.com/easy_quote_info/api/";
    private apiFUrl:string = "http://cingotest.com/easy_quote_info/apiF/get.php?funcName=";
    private apiFPostUrl:string = "http://cingotest.com/easy_quote_info/apiF/post.php?funcName=";

    constructor(private http: HttpClient) { }

    postData(url:string, data: any) {
        let options = this.createRequestOptions();
        return this.http.post(url, {data}, { headers: options })
            .map(res => res)
            .catch(this.handleErrors);
    }

    postFormData(url:string, data: any){
        // url="http://192.168.1.5:81/test.php";
        var formData = new FormData();
        Object.keys(data.formData).map(key=>{
            formData.append(key,data.formData[key]);
        });
        const headers = new HttpHeaders({ 'enctype': 'multipart/form-data' });
        return this.http.post(url, formData, { headers: headers })
            .map(res => res)
            .catch(this.handleErrors);
    }

    postDataFull(url:string, data: any) {
        let options = this.createRequestOptions();
        return this.http.post(url, {data}, { headers: options })
            .map(res => res)
            .catch(this.handleErrorsFull);
    }

    fileUpload(url:string, data:any): Promise<string> {
        // "/storage/emulated/0/Pictures/flower.jpeg", "flower", 'http://192.168.1.4:81/test.php'
        return new Promise((resolve, reject) => {
                const s = bghttp.session("file-upload");
                const req = {
                    url: url,
                    method: "POST",
                    headers: {
                        "Content-Type": "application/octet-stream",
                        "File-Name": data.jobFile.replace(/^.*[\\\/]/, '')
                    },
                    description:'uploading job file'
                };
                
                var params = [];
                if(data.formData){
                    Object.keys(data.formData).map(key=>{
                        params.push({name:""+key,value:""+data.formData[key]});
                        console.log(key+" "+data.formData[key]);
                    });
                }
                params.push({ name: "fileToUpload", filename: data.jobFile, mimeType: 'application/pdf'});
                const task = s.multipartUpload(params, req);
                let responseData = "";
                task.on("error", err => {
                    console.log("Error "+JSON.stringify(err));
                    reject(Error("Error uploading file"));
                });
                task.on("complete", res => {
                    console.log("Complete "+JSON.stringify(res));
                    resolve(responseData);
                });
                task.on("responded", (e) => {
                    responseData = responseData + e.data;
                    console.log("Responded "+JSON.stringify(e));
                });
        });
    }

    

    // postMultipartData(url:string, data:any, callBack:Function){
    //     // .subscribe(
    //     //     res => {
    //     //         callBack((<any>res).json.data);
    //     //     },
    //     //     () => {
    //     //         callBack ("Error while posting Job!");
    //     //     }
    //     // );
    //     var form_data = new FormData();
    //     for ( var key in data ) {
    //         form_data.append(key, data[key]);
    //     }

    //     var session = bghttp.session("image-upload");
        
    //     var request = {
    //         url:url,
    //         method: "POST",
    //         headers: {
    //             "Content-Type": "application/octet-stream",
    //             "File-Name": 'gib'
    //         },
    //         // description: description
    //     };
    
    //     // if (should_fail) {
    //     //     request.headers["Should-Fail"] = true;
    //     // }
    
    //     let task: bghttp.Task;
    //     var params = [
    //                 { name: "test", value: "value" },
    //                 { name: "fileToUpload", filename: '/storage/emulated/0/Pictures/flower.jpeg', mimeType: 'image/jpeg' }
    //             ];
    //     task = session.multipartUpload(params, request);
    //     task.on("progress", logEvent);
    //     task.on("error", logEvent);
    //     task.on("responded", logEvent);
    
    //     function logEvent(e) {
    //         // console.log("currentBytes: " + e.currentBytes);
    //         console.log("totalBytes: " + e.totalBytes);
    //         // console.log("eventName: " + e.eventName);
    //         console.log("Response: "+JSON.stringify(e));
    //     }
    // }

    getData(url:string){
        return this.http.get(url)
            .map(res => res)
            .catch(this.handleErrors);
    }

    getDataFull(url:string){
        return this.http.get(url)
            .map(res => res)
            .catch(this.handleErrorsFull);
    }

    private createRequestOptions() {
        let headers = new HttpHeaders();
        headers.set("AuthKey", "my-key");
        headers.set("AuthToken", "my-token");
        headers.set("Content-Type", "application/json");
        headers.set("Cache-Control", "no-cache");
        return headers;
    }

    handleErrors(error: any) {
        console.log(JSON.stringify(error));
        return Observable.throw(error);
    }

    handleErrorsFull(error: any) {
        console.log(JSON.stringify(error));
        if(error.status == '0')
            alert("Please connect to Internet!")
        else
            return Observable.throw(error);
    }


    //API CALLS
    public getCountryItems(data,callBack){
        return this.getData(this.apiFUrl+"service_finder_get_countries").subscribe(
            res => {
                callBack(res);
            },
            e => {
                console.log(JSON.stringify(e));
            }
        );
    }

    public getCategoryList(data,callBack){
        return this.getData(this.baseUrl+"providers-category?per_page=100").subscribe(
            res => {
                //console.log(JSON.stringify(res[1]))
                callBack(res);
            },
            e => {
                console.log(e);
            }
        );
    }

    public getCoutries(data,callBack){
        return this.getData(this.apiFUrl+"getCountries").subscribe(
            res => {
                callBack(res);
            },
            e => {
                console.log(e);
            }
        );
    }

    public getPodsOptions(data,callBack){
        return this.getData(this.apiFUrl+"getPodsOptions").subscribe(
            res => {
                callBack(res);
            },
            e => {
                console.log(e);
            }
        );
    }

    public postJob(data,callBack) {
        let url = this.apiFPostUrl+'submitJob';
        if(data.jobFile){
            this.fileUpload(url,data)
            .then(res => {
                callBack(res);
            })
            .catch(err => {
                callBack(err);
            });
        }else{
            this.postFormData(url,data)
            .subscribe(
                res => {
                    callBack(res);
                },
                () => {
                    callBack ("Error while submitting Job!");
                }
            );
        }
    }

    public postQuote(data,callBack) {
        let url = this.apiFPostUrl+'submitVendorQ';
        if(data.jobFile){
            this.fileUpload(url,data)
            .then(res => {
                callBack(res);
            })
            .catch(err => {
                callBack(err);
            });
        }
    }

    public loginUser(data,callBack) {
        let urlA:string = this.apiUrl + "get_nonce/?controller=auth&method=generate_auth_cookie";
        this.getDataFull(urlA).subscribe(
        res => {
            // alert(JSON.stringify(res));
            let nonce:string = res.nonce;
            if(nonce){
                urlA = this.apiUrl + "auth/generate_auth_cookie/?nonce="
                        + nonce
                        + "&username="
                        + data.username
                        +"&password="
                        + data.password
                        +"&insecure=cool";
            this.getDataFull(urlA).subscribe(
            res2 => {
                // alert(JSON.stringify(res2));
                let cookie:string = res2.cookie;
                
                if(cookie){
                let res2SavePoint = res2;
                
                urlA = this.apiUrl + "user/get_user_meta/?cookie="
                        + cookie
                        + "&insecure=cool";
                this.getDataFull(urlA).subscribe(
                res3 => {
                    // alert(JSON.stringify(res3));
                    let userType:string = "";
                    let userCat:string = "";
                    if(res3.status == "ok"){
                        var capab:string = res3.es_capabilities;
                        // alert(capab);
                        if(capab.includes("Customer")){
                            userType = AppConstants.CUSTOMER;
                        }else if(capab.includes("Provider")){
                            userType = AppConstants.PROVIDER;
                            userCat = res3.primary_category;
                        }
                        callBack({cookie:cookie,nonce:nonce,userType:userType,
                            userCat:userCat,userId:res2SavePoint.user.id,
                            userEmail:res2SavePoint.user.email,
                            userFirstName:res3.first_name,
                            usreLastName:res3.last_name,
                            userName:res3.first_name+" "+res3.last_name});
                    }
                    
                },
                (e) => {
                    callBack(e);
                }
                );
                }else{
                    callBack(res2);
                }
            },
            (e) => {
                callBack(e);
            }
            );
            }
        },
        (e) => {
            callBack(e);
        }
        );
    }

    public getJobListings(data,callBack){
        let aURL = this.apiFUrl + "getJobListing&jobId=" + data.catId;
        return this.getDataFull(aURL).subscribe(
            res => {
                callBack(res);
            },
            () => {
                alert ("Error while fetching job listings");
            }
        );
    }

    public getPostsByAuthor(data,callBack){
        let aURL = this.apiFUrl + "getPostsByAuthor&USER_ID=" + data.USER_ID;
        return this.getDataFull(aURL).subscribe(
            res => {
                callBack(res);
            },
            () => {
                alert ("Error while job posts");
            }
        );
    }

    public getQuotesForJob(data,callBack){
        let aURL = this.apiFUrl + "getQuotesForJob&JOB_ID=" + data.JOB_ID;
        return this.getDataFull(aURL).subscribe(
            res => {
                callBack(res);
            },
            () => {
                alert ("Error while job posts");
            }
        );
    }

    public getListingAuthor(data,callBack){
        let aURL = this.apiFUrl + "getListingAuthor&postId=" + data.postId;
        return this.getDataFull(aURL).subscribe(
            res => {
                callBack(res);
            },
            () => {
                alert ("Error while fetching job listings");
            }
        );
    }

    public findProvider(data,callBack){
        let aURL = this.apiFUrl + "findProvider"
                    +"&categoryId="+data.categoryId
                    +"&searchString="+data.searchString
                    +"&country="+data.country
                    +"&city=" + data.city;
                    console.log(aURL);
        return this.getDataFull(aURL).subscribe(
            res => {
                callBack(res);
            },
            () => {
                alert ("Error while searching!");
            }
        );
    }

    // public findProvider(data,callBack) {
    //     return this.postData(this.mirrorServerURL,data).subscribe(
    //         res => {
    //             callBack((<any>res).json.data);
    //         },
    //         () => {
    //             callBack ("Error while searching!");
    //         }
    //     );
    // }
    
}