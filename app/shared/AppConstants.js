"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AppConstants = /** @class */ (function () {
    function AppConstants() {
    }
    AppConstants.API_URL = "https://httpbin.org/post";
    AppConstants.BASE_URL = "http://www.cingotest.com/easy_quote_info/wp-json/wp/v2";
    AppConstants.PROVIDER = "PROVIDER";
    AppConstants.CUSTOMER = "CUSTOMER";
    return AppConstants;
}());
exports.AppConstants = AppConstants;
