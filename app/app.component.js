"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var angular_1 = require("nativescript-ui-sidedrawer/angular");
var nativescript_ui_sidedrawer_1 = require("nativescript-ui-sidedrawer");
var router_1 = require("nativescript-angular/router");
var StoreService_1 = require("./shared/service/StoreService");
var HttpServicePost_1 = require("./shared/service/HttpServicePost");
var CommonUtils_1 = require("./utils/CommonUtils");
var AppConstants_1 = require("./shared/AppConstants");
var AppComponent = /** @class */ (function () {
    function AppComponent(_changeDetectionRef, routerExtensions, storeService, post, utils) {
        this._changeDetectionRef = _changeDetectionRef;
        this.routerExtensions = routerExtensions;
        this.storeService = storeService;
        this.post = post;
        this.utils = utils;
        //ng-models
        this.userName = "";
        this.userType = "";
        this.isCustomer = false;
        this.isProvider = false;
        this.isLoading = false;
    }
    AppComponent.prototype.ngAfterViewInit = function () {
        this.drawer = this.drawerComponent.sideDrawer;
        this._changeDetectionRef.detectChanges();
        this.sideDrawerTransition = new nativescript_ui_sidedrawer_1.PushTransition();
    };
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isLoading = true;
        console.log("ngOnInit");
        this.storeService.categoryList = "[]";
        this.storeService.countryItems = "[]";
        //Load store values
        // let aCat = JSON.stringify(this.storeService.categoryList);
        // if(aCat.length == 0){
        this.post.getCategoryList("", function (msg) {
            _this.storeService.categoryList = JSON.stringify(msg);
            //Load Home Page  -- default to /home
            if (!_this.storeService.isLoggedIn)
                _this.routerExtensions.navigate(['/login']);
            else if (_this.storeService.userType == AppConstants_1.AppConstants.PROVIDER)
                _this.routerExtensions.navigate(['/jobAlerts']);
            else
                _this.routerExtensions.navigate(['/home']);
            _this.refreshData();
            _this.isLoading = false;
        });
        // }
    };
    AppComponent.prototype.ngOnDestroy = function () { };
    Object.defineProperty(AppComponent.prototype, "sideDrawerTransition", {
        get: function () {
            return this._sideDrawerTransition;
        },
        set: function (value) {
            this._sideDrawerTransition = value;
        },
        enumerable: true,
        configurable: true
    });
    AppComponent.prototype.toggleDrawer = function () {
        this.refreshData();
        this.drawer.toggleDrawerState();
    };
    AppComponent.prototype.btnSubmitQuote = function () {
        this.routerExtensions.navigate(["submitQuote"], { clearHistory: true });
        this.drawer.closeDrawer();
    };
    AppComponent.prototype.btnJobAlerts = function () {
        this.routerExtensions.navigate(["jobAlerts"], { clearHistory: false });
        this.drawer.closeDrawer();
    };
    AppComponent.prototype.btnMyJobs = function () {
        this.routerExtensions.navigate(["myJobs"], { clearHistory: false });
        this.drawer.closeDrawer();
    };
    AppComponent.prototype.btnHome = function () {
        this.routerExtensions.navigate(["home"], { clearHistory: true });
        this.drawer.closeDrawer();
    };
    AppComponent.prototype.btnLogin = function () {
        if (!this.storeService.isLoggedIn())
            this.routerExtensions.navigate(["login"], { clearHistory: true });
        this.drawer.closeDrawer();
    };
    AppComponent.prototype.logoff = function () {
        this.storeService.cookie = "";
        this.storeService.nonce = "";
        this.storeService.userName = "";
        this.storeService.userPass = "";
        this.storeService.userType = "";
        this.storeService.userId = "";
        this.storeService.categoryList = "";
        this.refreshData();
        this.btnLogin();
    };
    AppComponent.prototype.refreshData = function () {
        var loadStack = 1;
        this.userName = this.storeService.isLoggedIn() ? this.storeService.userName : "Login";
        this.userType = this.storeService.isLoggedIn() ? this.storeService.userType : "";
        this.isLoggedIn = this.storeService.isLoggedIn();
        this.isCustomer = (this.storeService.userType == AppConstants_1.AppConstants.CUSTOMER);
        this.isProvider = (this.storeService.userType == AppConstants_1.AppConstants.PROVIDER);
    };
    __decorate([
        core_1.ViewChild(angular_1.RadSideDrawerComponent),
        __metadata("design:type", angular_1.RadSideDrawerComponent)
    ], AppComponent.prototype, "drawerComponent", void 0);
    AppComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            templateUrl: "./app.html",
            styleUrls: ['./app.css']
        }),
        __metadata("design:paramtypes", [core_1.ChangeDetectorRef,
            router_1.RouterExtensions,
            StoreService_1.StoreService,
            HttpServicePost_1.HttpPostService,
            CommonUtils_1.CommonUtils])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
