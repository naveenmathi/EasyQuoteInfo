"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var frameModule = require("ui/frame");
var HttpServicePost_1 = require("../../shared/service/HttpServicePost");
var StoreService_1 = require("../../shared/service/StoreService");
var nativescript_drop_down_1 = require("nativescript-drop-down");
var CommonUtils_1 = require("../../utils/CommonUtils");
var router_1 = require("nativescript-angular/router");
var router_2 = require("@angular/router");
var topmost = frameModule.topmost();
var JobAlertsComponent = /** @class */ (function () {
    function JobAlertsComponent(post, store, utils, routerExtensions, route) {
        this.post = post;
        this.store = store;
        this.utils = utils;
        this.routerExtensions = routerExtensions;
        this.route = route;
        this.selectedIndex = 1;
        this.resultList = [];
        this.templateSelector = function (item, index, items) {
            if (index == 0)
                return item.type || "header";
            else
                return item.type || "cell";
        };
        //ngModels
        this.isLoading = false;
    }
    JobAlertsComponent.prototype.startLoading = function () {
        this.isLoading = true;
    };
    JobAlertsComponent.prototype.stopLoading = function () {
        this.isLoading = false;
    };
    JobAlertsComponent.prototype.ngOnInit = function () {
        var _this = this;
        //fetch job listings
        this.startLoading();
        this.post.getJobListings({ catId: this.store.userCat }, function (msg) {
            _this.stopLoading();
            _this.resultList = msg;
            // console.log(JSON.stringify(msg));
        });
        this.categoryItems = new nativescript_drop_down_1.ValueList();
        this.categoryItems.push({ value: "", display: "Select Category" });
        this.post.getCategoryList("", function (msg) {
            _this.store.categoryList = JSON.stringify(msg);
            JSON.parse(_this.store.categoryList).map(function (cat) {
                _this.categoryItems.push({ value: cat.id, display: _this.utils.parseAmp(cat.name) });
            });
            _this.isLoading = false;
            _this.store.userCatTxt = _this.getCategoryName(_this.store.userCat);
        });
    };
    JobAlertsComponent.prototype.ngOnDestroy = function () { };
    JobAlertsComponent.prototype.onClickList = function (args) {
        // console.log(JSON.stringify(this.resultList[args.index]));
        var selectedItem = JSON.stringify(this.resultList[args.index]);
        this.routerExtensions.navigate(["/submitVendorQ"], { queryParams: { selectedItem: selectedItem }, relativeTo: this.route });
    };
    JobAlertsComponent.prototype.getCategoryName = function (catId) {
        var catList = [];
        catList = JSON.parse(this.store.categoryList);
        return catList.find(function (o) { return o.id == catId; }).name;
    };
    JobAlertsComponent = __decorate([
        core_1.Component({
            templateUrl: "./pages/jobAlerts/jobAlerts.html",
            styleUrls: ["./pages/jobAlerts/jobAlerts-common.css"],
        }),
        __metadata("design:paramtypes", [HttpServicePost_1.HttpPostService,
            StoreService_1.StoreService,
            CommonUtils_1.CommonUtils,
            router_1.RouterExtensions,
            router_2.ActivatedRoute])
    ], JobAlertsComponent);
    return JobAlertsComponent;
}());
exports.JobAlertsComponent = JobAlertsComponent;
