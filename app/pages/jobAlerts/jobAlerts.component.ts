import { Component, OnInit, OnDestroy } from "@angular/core";
import * as frameModule from "ui/frame";
import { SelectedIndexChangedEventData } from "nativescript-drop-down";
import { HttpPostService } from "../../shared/service/HttpServicePost";
import { StoreService } from "../../shared/service/StoreService";
import { ValueList } from "nativescript-drop-down";
import view = require("ui/core/view");
import { CommonUtils } from "../../utils/CommonUtils";
import {RouterExtensions} from "nativescript-angular/router";
import { ActivatedRoute } from '@angular/router';

const topmost = frameModule.topmost();

@Component({
  templateUrl:"./pages/jobAlerts/jobAlerts.html",
  styleUrls:["./pages/jobAlerts/jobAlerts-common.css"],
})
export class JobAlertsComponent  implements OnInit, OnDestroy {
  private selectedIndex = 1;
  private categoryItems: ValueList<string>;
  private searchKeyword:string;
  private resultList = [];

  public templateSelector = (item: any, index: number, items: any) => {
    if(index == 0)
      return item.type || "header"
    else
      return item.type || "cell";
  }

  //ngModels
  isLoading:boolean = false;
  
  startLoading(){
    this.isLoading = true;
  }
  stopLoading(){
    this.isLoading = false;
  }

  constructor(private post: HttpPostService,
    private store:StoreService,
    private utils: CommonUtils,
    private routerExtensions:RouterExtensions,
    private route: ActivatedRoute){

  }

  ngOnInit() {
      //fetch job listings
      this.startLoading();
      this.post.getJobListings({catId:this.store.userCat}, (msg) => {
        this.stopLoading();
        this.resultList = msg;
        // console.log(JSON.stringify(msg));
      });

      this.categoryItems = new ValueList<string>();
      this.categoryItems.push({value:"",display:"Select Category"});
      this.post.getCategoryList("", msg => {
        this.store.categoryList = JSON.stringify(msg);
        JSON.parse(this.store.categoryList).map(cat => {
          this.categoryItems.push({ value: cat.id, display: this.utils.parseAmp(cat.name) });
        });
        this.isLoading = false;
        this.store.userCatTxt=this.getCategoryName(this.store.userCat);
      });


      
  }    

  ngOnDestroy(){}

  public onClickList(args){
    // console.log(JSON.stringify(this.resultList[args.index]));
    var selectedItem:String = JSON.stringify(this.resultList[args.index]);
    this.routerExtensions.navigate(["/submitVendorQ"],{ queryParams: { selectedItem:selectedItem  },  relativeTo: this.route});
  }

  getCategoryName(catId:string) :string{
    let catList = [];
    catList = JSON.parse(this.store.categoryList);
    return catList.find(o => o.id == catId).name;
  }

}