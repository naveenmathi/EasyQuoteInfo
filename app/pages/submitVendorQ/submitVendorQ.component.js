"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var frameModule = require("ui/frame");
var HttpServicePost_1 = require("../../shared/service/HttpServicePost");
var StoreService_1 = require("../../shared/service/StoreService");
var CommonUtils_1 = require("../../utils/CommonUtils");
var app = require("application");
var router_2 = require("nativescript-angular/router");
var platform_1 = require("platform");
// import { Mediafilepicker, MediaFilepickerOptions } from 'nativescript-mediafilepicker';
var nativescript_mediafilepicker_1 = require("nativescript-mediafilepicker");
var topmost = frameModule.topmost();
var SubmitVendorQComponent = /** @class */ (function () {
    // private mediafilepicker: Mediafilepicker;
    function SubmitVendorQComponent(post, store, routerExtensions, utils, route, zone) {
        this.post = post;
        this.store = store;
        this.routerExtensions = routerExtensions;
        this.utils = utils;
        this.route = route;
        this.zone = zone;
        this.selectedIndex = 1;
        this.resultList = [];
        this.isLoading = false;
        this.listingDetails = {};
        this.postTitle = "";
        this.jobFile = "";
        this.currentItem = "";
        this.jobComments = "";
        this.templateSelector = function (item, index, items) {
            if (index == 0)
                return item.type || "header";
            else
                return item.type || "cell";
        };
    }
    SubmitVendorQComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.listingDetails = { userCatTxt: this.store.userCatTxt };
        this.route.queryParams.subscribe(function (params) {
            var selectedItem = JSON.parse(params.selectedItem);
            if (selectedItem) {
                _this.postTitle = selectedItem.post_title;
                _this.post.getListingAuthor({ postId: selectedItem.ID }, function (msg) {
                    if (msg) {
                        // console.log(JSON.stringify(msg));
                        _this.currentItem = selectedItem.ID;
                        _this.listingDetails = Object.assign(_this.listingDetails, msg);
                        _this.listingDetails.post_content = selectedItem.post_content;
                        _this.listingDetails.userCatTxt = _this.store.userCatTxt;
                        _this.listingDetails.vehicle_type = 'Vehicle Type : ' + _this.listingDetails.vehicle_type;
                        _this.listingDetails.vehicle_value = 'Vehicle Value : ' + _this.listingDetails.vehicle_value;
                        _this.listingDetails.engine_size = 'Engine Size : ' + _this.listingDetails.engine_size;
                        _this.listingDetails.driver_experience = 'Driver Experience : ' + _this.listingDetails.driver_experience;
                        _this.listingDetails.accidents_in_the_last_5_years = 'Accident in last 5 years : ' + _this.listingDetails.accidents_in_the_last_5_years;
                        _this.listingDetails.no_claims_discount = 'No Claims Discount : ' + _this.listingDetails.no_claims_discount;
                        _this.listingDetails.driver_age = 'Driver Age : ' + _this.listingDetails.driver_age;
                        _this.listingDetails.current_insurer = 'Current Insurer : ' + _this.listingDetails.current_insurer;
                        _this.listingDetails.home_insurance_type = 'Home Insurance Type : ' + _this.listingDetails.home_insurance_type;
                        _this.listingDetails.total_value_to_insure_bds = 'Total value to insure(BDS) : ' + _this.listingDetails.total_value_to_insure_bds;
                        _this.listingDetails.business_name = 'Business Name : ' + _this.listingDetails.business_name;
                        _this.listingDetails.business_type = 'Business Type : ' + _this.listingDetails.business_type;
                    }
                });
            }
        });
        if (!platform_1.isAndroid) {
            return;
        }
        // app.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        //   if (this.routerExtensions.router.isActive("/submitVendorQ", false)) {
        //     data.cancel = true;   // prevents default back button behavior
        //     this.routerExtensions.navigate(["/jobAlerts"]);
        //   }
        // });
    };
    SubmitVendorQComponent.prototype.ngOnDestroy = function () {
    };
    SubmitVendorQComponent.prototype.dummyClick = function () {
        this.routerExtensions.navigate(["/jobAlerts"]);
    };
    SubmitVendorQComponent.prototype.getImageHeight = function () {
        if (this.listingDetails.image)
            return 200;
        else
            return 0;
    };
    SubmitVendorQComponent.prototype.openFilePicker = function () {
        // this.isLoading = true;
        var extensions = [];
        if (app.ios) {
            extensions = ['txt', 'pdf'];
            // extensions = [kUTTypePDF, kUTTypeText]; //you can get more types from here: https://developer.apple.com/documentation/mobilecoreservices/uttype
        }
        else if (app.android) {
            extensions = ['jpg', 'jpeg', 'png'];
        }
        var options = {
            android: {
                extensions: extensions,
                maxNumberFiles: 1,
            },
            ios: {
                extensions: extensions,
                multipleSelection: false
            }
        };
        var imgOptions = {
            android: {
                isNeedCamera: true,
                maxNumberFiles: 1,
                isNeedFolderList: true
            },
            ios: {
                isCaptureMood: false,
                maxNumberFiles: 1
            }
        };
        var mediafilepicker = new nativescript_mediafilepicker_1.Mediafilepicker();
        // mediafilepicker.openFilePicker(options);
        mediafilepicker.openImagePicker(imgOptions);
        var that = this;
        mediafilepicker.on("getFiles", function (res) {
            this.isLoading = false;
            var results = res.object.get('results');
            that.jobFile = results[0].file;
            that.zone.run(function () {
                that.jobFile = that.jobFile;
            });
            console.log(results[0].file);
        });
        mediafilepicker.on("error", function (res) {
            this.isLoading = false;
            var msg = res.object.get('msg');
            console.log(msg);
        });
        mediafilepicker.on("cancel", function (res) {
            this.isLoading = false;
            var msg = res.object.get('msg');
            console.log(msg);
        });
    };
    SubmitVendorQComponent.prototype.btnPostJob = function () {
        var _this = this;
        this.isLoading = true;
        var formData = Object.assign(this.listingDetails, { postTitle: this.jobComments,
            currentItem: this.currentItem,
            currentUserID: this.store.userId,
            provName: this.store.userName,
            provEmail: this.store.userMail,
        });
        var data = {
            formData: formData,
            jobFile: this.jobFile,
        };
        this.post.postQuote(data, function (res) {
            console.log("Result------------" + JSON.stringify(res));
            var response = JSON.stringify(res).replace("\\", "");
            if (response.includes("already submitted quote")) {
                alert("ERROR: You've already submitted quote for this job");
            }
            else if (response.includes("Quote posted successfully")) {
                alert("Quote posted successfully");
            }
            else if (response.includes("Error no file selected")) {
                alert("Error no file selected");
            }
            else {
                alert("Something went wrong!");
            }
            _this.clearData();
            _this.isLoading = false;
        });
    };
    SubmitVendorQComponent.prototype.clearData = function () {
        this.jobFile = "";
    };
    SubmitVendorQComponent = __decorate([
        core_1.Component({
            templateUrl: "./pages/submitVendorQ/submitVendorQ.html",
            styleUrls: ["./pages/submitVendorQ/submitVendorQ-common.css"],
        }),
        __metadata("design:paramtypes", [HttpServicePost_1.HttpPostService,
            StoreService_1.StoreService,
            router_2.RouterExtensions,
            CommonUtils_1.CommonUtils,
            router_1.ActivatedRoute,
            core_1.NgZone])
    ], SubmitVendorQComponent);
    return SubmitVendorQComponent;
}());
exports.SubmitVendorQComponent = SubmitVendorQComponent;
