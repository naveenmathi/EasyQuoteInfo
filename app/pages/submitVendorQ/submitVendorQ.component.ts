import { Component, NgZone, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import * as frameModule from "ui/frame";
import { SelectedIndexChangedEventData } from "nativescript-drop-down";
import { HttpPostService } from "../../shared/service/HttpServicePost";
import { StoreService } from "../../shared/service/StoreService";
import { ValueList } from "nativescript-drop-down";
import view = require("ui/core/view");
import { CommonUtils } from "../../utils/CommonUtils";
import * as fs from "file-system"
import * as app from 'application';
import {RouterExtensions} from "nativescript-angular/router";
import { AndroidApplication, AndroidActivityBackPressedEventData } from "application";
import { isAndroid } from "platform";
// import { Mediafilepicker, MediaFilepickerOptions } from 'nativescript-mediafilepicker';
import { Mediafilepicker, FilePickerOptions, ImagePickerOptions } from 'nativescript-mediafilepicker';

const topmost = frameModule.topmost();

@Component({
  templateUrl:"./pages/submitVendorQ/submitVendorQ.html",
  styleUrls:["./pages/submitVendorQ/submitVendorQ-common.css"],
})
export class SubmitVendorQComponent implements OnInit, OnDestroy{
  private selectedIndex = 1;
  private categoryItems: ValueList<string>;
  private searchKeyword:string;
  private resultList = [];
  isLoading:boolean = false;
  listingDetails:any = {};
  postTitle:String = "";
  jobFile:string = "";
  currentItem:string = "";
  jobComments:string = "";
  // private mediafilepicker: Mediafilepicker;

  constructor(private post: HttpPostService,
    private store:StoreService,
    private routerExtensions:RouterExtensions,
    private utils: CommonUtils,
    private route: ActivatedRoute,
    private zone:NgZone){
  }
  
  ngOnInit() {
    this.listingDetails = {userCatTxt:this.store.userCatTxt};
    this.route.queryParams.subscribe(params => {
      var selectedItem = JSON.parse(params.selectedItem);
      if(selectedItem){
        this.postTitle = selectedItem.post_title;
        this.post.getListingAuthor({postId:selectedItem.ID},(msg) => {
          if(msg){
            // console.log(JSON.stringify(msg));
            this.currentItem = selectedItem.ID;
            this.listingDetails = Object.assign(this.listingDetails,msg);
            this.listingDetails.post_content = selectedItem.post_content;
            this.listingDetails.userCatTxt = this.store.userCatTxt;
            this.listingDetails.vehicle_type = 'Vehicle Type : ' + this.listingDetails.vehicle_type;
            this.listingDetails.vehicle_value = 'Vehicle Value : ' + this.listingDetails.vehicle_value;
            this.listingDetails.engine_size = 'Engine Size : ' + this.listingDetails.engine_size;
            this.listingDetails.driver_experience = 'Driver Experience : ' + this.listingDetails.driver_experience;
            this.listingDetails.accidents_in_the_last_5_years = 'Accident in last 5 years : ' + this.listingDetails.accidents_in_the_last_5_years;
            this.listingDetails.no_claims_discount = 'No Claims Discount : ' + this.listingDetails.no_claims_discount;
            this.listingDetails.driver_age = 'Driver Age : ' + this.listingDetails.driver_age;
            this.listingDetails.current_insurer = 'Current Insurer : ' + this.listingDetails.current_insurer;
            this.listingDetails.home_insurance_type = 'Home Insurance Type : ' + this.listingDetails.home_insurance_type;
            this.listingDetails.total_value_to_insure_bds = 'Total value to insure(BDS) : ' + this.listingDetails.total_value_to_insure_bds;
            this.listingDetails.business_name = 'Business Name : ' + this.listingDetails.business_name;
            this.listingDetails.business_type = 'Business Type : ' + this.listingDetails.business_type;
          }
        });
      }
    });

    if (!isAndroid) {
      return;
    }
    // app.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
    //   if (this.routerExtensions.router.isActive("/submitVendorQ", false)) {
    //     data.cancel = true;   // prevents default back button behavior
    //     this.routerExtensions.navigate(["/jobAlerts"]);
    //   }
    // });
  } 

  ngOnDestroy() {
  }

  public templateSelector = (item: any, index: number, items: any) => {
    if(index == 0)
      return item.type || "header"
    else
      return item.type || "cell";
  }

  public dummyClick(){
    this.routerExtensions.navigate(["/jobAlerts"]);
  }

  getImageHeight(){
    if(this.listingDetails.image)
      return 200;
    else
      return 0;
  }

  openFilePicker() {
    // this.isLoading = true;
    let extensions = [];
    if (app.ios) {
      extensions = ['txt', 'pdf'];
      // extensions = [kUTTypePDF, kUTTypeText]; //you can get more types from here: https://developer.apple.com/documentation/mobilecoreservices/uttype
    } else if(app.android) {
      extensions = ['jpg', 'jpeg', 'png'];
    }
    let options: FilePickerOptions = {
      android: {
        extensions: extensions,
        maxNumberFiles: 1,
      },
      ios: {
        extensions: extensions,
        multipleSelection: false
      }
    };
    let imgOptions: ImagePickerOptions = {
      android: {
        isNeedCamera: true,
        maxNumberFiles: 1,
        isNeedFolderList: true
      },
      ios: {
        isCaptureMood: false,
        maxNumberFiles: 1
      }
    };
    let mediafilepicker = new Mediafilepicker();
    // mediafilepicker.openFilePicker(options);
    mediafilepicker.openImagePicker(imgOptions);
    var that = this;
    mediafilepicker.on("getFiles", function (res) {
      this.isLoading = false;
      let results = res.object.get('results');
      that.jobFile = results[0].file;
      that.zone.run(() =>{
        that.jobFile = that.jobFile;
      });   
      console.log(results[0].file);
    });
    mediafilepicker.on("error", function (res) {
      this.isLoading = false;
      let msg = res.object.get('msg');
      console.log(msg);
    });
    mediafilepicker.on("cancel", function (res) {
      this.isLoading = false;
      let msg = res.object.get('msg');
      console.log(msg);
    });
  }

  btnPostJob() {
    this.isLoading = true;
    let formData = Object.assign(this.listingDetails,
      { postTitle: this.jobComments,
        currentItem:this.currentItem,
        currentUserID:this.store.userId,
        provName:this.store.userName,
        provEmail:this.store.userMail,
      });
    var data = {
      formData: formData,
      jobFile: this.jobFile,
    }
    this.post.postQuote(data, res => {
      console.log("Result------------"+JSON.stringify(res));
      let response = JSON.stringify(res).replace("\\","");
      if(response.includes("already submitted quote")){
        alert("ERROR: You've already submitted quote for this job");
      }else if(response.includes("Quote posted successfully")){
        alert("Quote posted successfully");
      }else if(response.includes("Error no file selected")){
        alert("Error no file selected");
      }else{
        alert("Something went wrong!");
      }
      this.clearData();
      this.isLoading = false;
    });
  }

  clearData() {
    this.jobFile = "";
  }

  // uploadFile(){
  //   let options: MediaFilepickerOptions = {
  //     android: {
  //         mxcount: 1,
  //         pickFile: true,
  //         enableDocSupport: true,
  //     },
  //     ios: {
  //         allowsMultipleSelection: false,
  //         title: "Album",
  //         showCameraButton: true,
  //     }
  //   };

  //   this.mediafilepicker = new Mediafilepicker();
  //   this.mediafilepicker.on("getFiles", function (res: any) {

  //       let files = res.files;

  //       if (files.length > 0) {

  //           files = files.split(",");

  //           files.forEach(file => {

  //               let fileName = file.replace(/^.*[\/]/, '');

  //               console.log(file);
  //               console.log(fileName);

  //               // you can do anything here
  //               if (app.ios) {

  //                   let folder = fs.knownFolders.documents();
  //                   let file = folder.getFolder("filepicker").getFile(fileName);

  //                   if (fs.File.exists(file.path)) {
  //                       folder.getFile("filepicker/" + fileName).remove()
  //                   } else {
  //                       console.log("not found")
  //                   }
  //               }
  //           });
  //       } else {
  //           console.log("There was some problem to select the file. Looks like user has cancel it.")
  //       }

  //   })
  //   this.mediafilepicker.startFilePicker(options);

  // }
}