import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { HttpPostService } from "../../shared/service/HttpServicePost";
import { AppConstants } from "../../shared/AppConstants";
import { StoreService } from "../../shared/service/StoreService";
import { Page, ShownModallyData, NavigatedData } from "tns-core-modules/ui/page";
import * as frameModule from "ui/frame";
import {RouterExtensions} from "nativescript-angular/router";
import * as bghttp from "nativescript-background-http";
import * as fs from "tns-core-modules/file-system";

const topmost = frameModule.topmost();

@Component({
  templateUrl:"./pages/login/login.html",
  styleUrls:["./pages/login/login-common.css", "./pages/login/login.css"],
})
export class LoginComponent implements OnInit, OnDestroy {

  username:string = "";
  password:string = "";
  isSecure:boolean = true;
  isLoading:boolean = false;
  isWelcomeMessage:boolean = false;
  loginMessage:string = "Welcome";

  constructor(private router:Router,
     private post: HttpPostService,
     private store: StoreService,
     page: Page,
     private routerExtensions: RouterExtensions){
  }

  ngOnInit(){
    
  }

  ngOnDestroy(){}

  startLoading(){
    this.isLoading = true;
  }
  stopLoading(){
    this.isLoading = false;
  }

  btnSignIn() {
    var data = {username:this.username,password:this.password};
    this.startLoading();
    this.post.loginUser(data,
      (msg) => {
        // alert(JSON.stringify(msg));
        this.stopLoading();
        if(msg.userType){
          this.store.userName = data.username;
          this.store.userPass = data.password;
          this.store.nonce=msg.nonce;
          this.store.cookie=msg.cookie;
          this.store.userType=msg.userType;
          this.store.userCat=msg.userCat;
          this.store.userId=msg.userId;
          this.store.userMail=msg.userEmail;
          this.store.firstName=msg.userFirstName;
          this.store.lastName=msg.usreLastName;
          this.loginMessage = msg.userName.toUpperCase();
          this.isWelcomeMessage=true;
          let that = this;
          setTimeout(function(){
            that.isWelcomeMessage = false;
            if(that.store.userType == AppConstants.CUSTOMER){
              that.routerExtensions.navigate(["submitQuote"], { clearHistory: false });
            }else{
              that.routerExtensions.navigate(["jobAlerts"], { clearHistory: false });
              // that.store.userCatTxt=that.getCategoryName(msg.userCat);
            }
          }, 2500);
        }else if(msg.error.status == 'error' || msg.status=='error'){
          alert("Invalid credentials!");
        }else{
          alert("Something went wrong!");
        }
      });
  }

  

  btnSignUp(){
    this.routerExtensions.navigate(['/signup']);
  }

  btnGoBack(){
    this.routerExtensions.navigate(['/home']);
  }

  btnUplaod(){
    var documents = fs.knownFolders.documents();
    var imgPath = fs.knownFolders.currentApp().path + "/images/vintage.jpg";
    alert(imgPath);
    //alert(documents.path);
    var api_url = 'http://cingotest.com/prepool/admin/mobile_image_upload_check.php';
    var session = bghttp.session("image-upload");
    var request = {
      url: api_url,
      method: "POST",
      headers: {
        "Content-Type": "application/octet-stream",
        "File-Name": "logo.png"
      },
      description: 'description',
    };
  
    var should_fail = false;
    if (should_fail) {
      request.headers["Should-Fail"] = true;
    }
  
  let task: bghttp.Task;

  var params = [
    { name: "test", value: "value" },
    { name: "IMAGE1", filename: imgPath, mimeType: 'image/jpeg' }
  ];

  task = session.multipartUpload(params, request);
  }
  
}