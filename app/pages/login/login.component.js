"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var HttpServicePost_1 = require("../../shared/service/HttpServicePost");
var AppConstants_1 = require("../../shared/AppConstants");
var StoreService_1 = require("../../shared/service/StoreService");
var page_1 = require("tns-core-modules/ui/page");
var frameModule = require("ui/frame");
var router_2 = require("nativescript-angular/router");
var bghttp = require("nativescript-background-http");
var fs = require("tns-core-modules/file-system");
var topmost = frameModule.topmost();
var LoginComponent = /** @class */ (function () {
    function LoginComponent(router, post, store, page, routerExtensions) {
        this.router = router;
        this.post = post;
        this.store = store;
        this.routerExtensions = routerExtensions;
        this.username = "";
        this.password = "";
        this.isSecure = true;
        this.isLoading = false;
        this.isWelcomeMessage = false;
        this.loginMessage = "Welcome";
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.ngOnDestroy = function () { };
    LoginComponent.prototype.startLoading = function () {
        this.isLoading = true;
    };
    LoginComponent.prototype.stopLoading = function () {
        this.isLoading = false;
    };
    LoginComponent.prototype.btnSignIn = function () {
        var _this = this;
        var data = { username: this.username, password: this.password };
        this.startLoading();
        this.post.loginUser(data, function (msg) {
            // alert(JSON.stringify(msg));
            _this.stopLoading();
            if (msg.userType) {
                _this.store.userName = data.username;
                _this.store.userPass = data.password;
                _this.store.nonce = msg.nonce;
                _this.store.cookie = msg.cookie;
                _this.store.userType = msg.userType;
                _this.store.userCat = msg.userCat;
                _this.store.userId = msg.userId;
                _this.store.userMail = msg.userEmail;
                _this.store.firstName = msg.userFirstName;
                _this.store.lastName = msg.usreLastName;
                _this.loginMessage = msg.userName.toUpperCase();
                _this.isWelcomeMessage = true;
                var that_1 = _this;
                setTimeout(function () {
                    that_1.isWelcomeMessage = false;
                    if (that_1.store.userType == AppConstants_1.AppConstants.CUSTOMER) {
                        that_1.routerExtensions.navigate(["submitQuote"], { clearHistory: false });
                    }
                    else {
                        that_1.routerExtensions.navigate(["jobAlerts"], { clearHistory: false });
                        // that.store.userCatTxt=that.getCategoryName(msg.userCat);
                    }
                }, 2500);
            }
            else if (msg.error.status == 'error' || msg.status == 'error') {
                alert("Invalid credentials!");
            }
            else {
                alert("Something went wrong!");
            }
        });
    };
    LoginComponent.prototype.btnSignUp = function () {
        this.routerExtensions.navigate(['/signup']);
    };
    LoginComponent.prototype.btnGoBack = function () {
        this.routerExtensions.navigate(['/home']);
    };
    LoginComponent.prototype.btnUplaod = function () {
        var documents = fs.knownFolders.documents();
        var imgPath = fs.knownFolders.currentApp().path + "/images/vintage.jpg";
        alert(imgPath);
        //alert(documents.path);
        var api_url = 'http://cingotest.com/prepool/admin/mobile_image_upload_check.php';
        var session = bghttp.session("image-upload");
        var request = {
            url: api_url,
            method: "POST",
            headers: {
                "Content-Type": "application/octet-stream",
                "File-Name": "logo.png"
            },
            description: 'description',
        };
        var should_fail = false;
        if (should_fail) {
            request.headers["Should-Fail"] = true;
        }
        var task;
        var params = [
            { name: "test", value: "value" },
            { name: "IMAGE1", filename: imgPath, mimeType: 'image/jpeg' }
        ];
        task = session.multipartUpload(params, request);
    };
    LoginComponent = __decorate([
        core_1.Component({
            templateUrl: "./pages/login/login.html",
            styleUrls: ["./pages/login/login-common.css", "./pages/login/login.css"],
        }),
        __metadata("design:paramtypes", [router_1.Router,
            HttpServicePost_1.HttpPostService,
            StoreService_1.StoreService,
            page_1.Page,
            router_2.RouterExtensions])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
