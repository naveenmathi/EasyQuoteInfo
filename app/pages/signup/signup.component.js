"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var HttpServicePost_1 = require("../../shared/service/HttpServicePost");
var StoreService_1 = require("../../shared/service/StoreService");
var page_1 = require("tns-core-modules/ui/page");
var frameModule = require("ui/frame");
var router_2 = require("nativescript-angular/router");
var nativescript_drop_down_1 = require("nativescript-drop-down");
var CommonUtils_1 = require("../../utils/CommonUtils");
var User_1 = require("../../shared/objects/User");
var topmost = frameModule.topmost();
var SignupComponent = /** @class */ (function () {
    function SignupComponent(router, post, page, store, routerExtensions, utils) {
        var _this = this;
        this.router = router;
        this.post = post;
        this.store = store;
        this.routerExtensions = routerExtensions;
        this.utils = utils;
        //ngModels 
        this.userM = new User_1.User;
        //initialize category
        this.categoryItems = new nativescript_drop_down_1.ValueList();
        this.categoryItems.push({ value: "-1", display: "Category:" });
        JSON.parse(this.store.categoryList).map(function (cat) {
            _this.categoryItems.push({ value: cat.id, display: utils.parseAmp(cat.name) });
        });
        //initialize category
        var self = this;
        this.countryItems = new nativescript_drop_down_1.ValueList();
        var countryItemObj = JSON.parse(this.store.countryItems);
        Object.keys(countryItemObj).map(function (key, index) {
            // console.log(key);
            self.countryItems.push({ value: key, display: utils.parseAmp(countryItemObj[key]) });
        });
    }
    SignupComponent.prototype.btnSignUp = function () {
    };
    SignupComponent = __decorate([
        core_1.Component({
            templateUrl: "./pages/signup/signup.html",
            styleUrls: ["./pages/signup/signup-common.css"],
        }),
        __metadata("design:paramtypes", [router_1.Router,
            HttpServicePost_1.HttpPostService,
            page_1.Page,
            StoreService_1.StoreService,
            router_2.RouterExtensions,
            CommonUtils_1.CommonUtils])
    ], SignupComponent);
    return SignupComponent;
}());
exports.SignupComponent = SignupComponent;
