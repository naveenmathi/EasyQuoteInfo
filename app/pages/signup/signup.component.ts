import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { HttpPostService } from "../../shared/service/HttpServicePost";
import { AppConstants } from "../../shared/AppConstants";
import { StoreService } from "../../shared/service/StoreService";
import { Page, ShownModallyData, NavigatedData } from "tns-core-modules/ui/page";
import * as frameModule from "ui/frame";
import {RouterExtensions} from "nativescript-angular/router";
import * as bghttp from "nativescript-background-http";
import * as fs from "tns-core-modules/file-system";
import { ValueList } from "nativescript-drop-down";
import { CommonUtils } from "../../utils/CommonUtils";
import {User} from "../../shared/objects/User";

const topmost = frameModule.topmost();

@Component({
  templateUrl:"./pages/signup/signup.html",
  styleUrls:["./pages/signup/signup-common.css"],
})
export class SignupComponent {

  private categoryItems: ValueList<string>;
  private countryItems: ValueList<string>;

  constructor(private router:Router,
     private post: HttpPostService,
     page: Page,
     private store:StoreService,
     private routerExtensions: RouterExtensions,
     private utils: CommonUtils){
       
    //initialize category
    this.categoryItems = new ValueList<string>();
    this.categoryItems.push({value: "-1", display: "Category:"});
    JSON.parse(this.store.categoryList).map(cat => {
       this.categoryItems.push({value: cat.id, display: utils.parseAmp(cat.name)});
    });

    //initialize category
    var self = this;
    this.countryItems = new ValueList<string>();
    let countryItemObj = JSON.parse(this.store.countryItems);
    Object.keys(countryItemObj).map(function(key, index) {
      // console.log(key);
      self.countryItems.push({value: key, display: utils.parseAmp(countryItemObj[key])});
    });
  }

  //ngModels 
  userM:User = new User;

  btnSignUp(){
    
  }
}