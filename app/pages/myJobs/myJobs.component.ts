import { Component, OnInit, OnDestroy } from "@angular/core";
import * as frameModule from "ui/frame";
import { SelectedIndexChangedEventData } from "nativescript-drop-down";
import { HttpPostService } from "../../shared/service/HttpServicePost";
import { StoreService } from "../../shared/service/StoreService";
import { ValueList } from "nativescript-drop-down";
import view = require("ui/core/view");
import { CommonUtils } from "../../utils/CommonUtils";
import {RouterExtensions} from "nativescript-angular/router";
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';

const topmost = frameModule.topmost();


@Component({
  templateUrl:"./pages/myJobs/myJobs.html",
  styleUrls:["./pages/myJobs/myJobs-common.css"],
})
export class MyJobsComponent  implements OnInit, OnDestroy {
  private selectedIndex = 1;
  private categoryItems: ValueList<string>;
  private searchKeyword:string;
  private resultList = [];

  public templateSelector = (item: any, index: number, items: any) => {
    if(index == 0)
      return item.type || "header"
    else
      return item.type || "cell";
  }

  //ngModels
  isLoading:boolean = false;
  
  startLoading(){
    this.isLoading = true;
  }
  stopLoading(){
    this.isLoading = false;
  }

  constructor(private post: HttpPostService,
    private store:StoreService,
    private utils: CommonUtils,
    private routerExtensions:RouterExtensions,
    private route: ActivatedRoute){

  }

  ngOnInit() {
      //fetch job listings
      this.startLoading();
      this.post.getPostsByAuthor({USER_ID:this.store.userId}, (msg) => {
        this.stopLoading();
        msg.forEach(element => {
          element.varPostDate = 'Posted: '+ moment(element.post_date,'YYYY-MM-DD').format('MMM DD YYYY');
          element.varExpDate = 'Expiry: '+ moment(element.post_date,'YYYY-MM-DD').add(1,'month').format('MMM DD YYYY');
          element.varAppliedCount = 'Applications: 0';
          this.resultList.push(element);
        });
      });
  }    

ngOnDestroy(){}

  public onClickList(args){
    // console.log(JSON.stringify(this.resultList[args.index]));
    var selectedItem:String = JSON.stringify(this.resultList[args.index]);
    this.routerExtensions.navigate(["/listQuotes"],{ queryParams: { selectedItem:selectedItem  },  relativeTo: this.route});
  }

  

}