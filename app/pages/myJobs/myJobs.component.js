"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var frameModule = require("ui/frame");
var HttpServicePost_1 = require("../../shared/service/HttpServicePost");
var StoreService_1 = require("../../shared/service/StoreService");
var CommonUtils_1 = require("../../utils/CommonUtils");
var router_1 = require("nativescript-angular/router");
var router_2 = require("@angular/router");
var moment = require("moment");
var topmost = frameModule.topmost();
var MyJobsComponent = /** @class */ (function () {
    function MyJobsComponent(post, store, utils, routerExtensions, route) {
        this.post = post;
        this.store = store;
        this.utils = utils;
        this.routerExtensions = routerExtensions;
        this.route = route;
        this.selectedIndex = 1;
        this.resultList = [];
        this.templateSelector = function (item, index, items) {
            if (index == 0)
                return item.type || "header";
            else
                return item.type || "cell";
        };
        //ngModels
        this.isLoading = false;
    }
    MyJobsComponent.prototype.startLoading = function () {
        this.isLoading = true;
    };
    MyJobsComponent.prototype.stopLoading = function () {
        this.isLoading = false;
    };
    MyJobsComponent.prototype.ngOnInit = function () {
        var _this = this;
        //fetch job listings
        this.startLoading();
        this.post.getPostsByAuthor({ USER_ID: this.store.userId }, function (msg) {
            _this.stopLoading();
            msg.forEach(function (element) {
                element.varPostDate = 'Posted: ' + moment(element.post_date, 'YYYY-MM-DD').format('MMM DD YYYY');
                element.varExpDate = 'Expiry: ' + moment(element.post_date, 'YYYY-MM-DD').add(1, 'month').format('MMM DD YYYY');
                element.varAppliedCount = 'Applications: 0';
                _this.resultList.push(element);
            });
        });
    };
    MyJobsComponent.prototype.ngOnDestroy = function () { };
    MyJobsComponent.prototype.onClickList = function (args) {
        // console.log(JSON.stringify(this.resultList[args.index]));
        var selectedItem = JSON.stringify(this.resultList[args.index]);
        this.routerExtensions.navigate(["/listQuotes"], { queryParams: { selectedItem: selectedItem }, relativeTo: this.route });
    };
    MyJobsComponent = __decorate([
        core_1.Component({
            templateUrl: "./pages/myJobs/myJobs.html",
            styleUrls: ["./pages/myJobs/myJobs-common.css"],
        }),
        __metadata("design:paramtypes", [HttpServicePost_1.HttpPostService,
            StoreService_1.StoreService,
            CommonUtils_1.CommonUtils,
            router_1.RouterExtensions,
            router_2.ActivatedRoute])
    ], MyJobsComponent);
    return MyJobsComponent;
}());
exports.MyJobsComponent = MyJobsComponent;
