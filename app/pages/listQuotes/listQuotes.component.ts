import { Component, OnInit, OnDestroy } from "@angular/core";
import * as frameModule from "ui/frame";
import { SelectedIndexChangedEventData } from "nativescript-drop-down";
import { HttpPostService } from "../../shared/service/HttpServicePost";
import { StoreService } from "../../shared/service/StoreService";
import { ValueList } from "nativescript-drop-down";
import view = require("ui/core/view");
import { CommonUtils } from "../../utils/CommonUtils";
import {RouterExtensions} from "nativescript-angular/router";
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import * as utils from "tns-core-modules/utils/utils";

const topmost = frameModule.topmost();


@Component({
  templateUrl:"./pages/listQuotes/listQuotes.html",
  styleUrls:["./pages/listQuotes/listQuotes-common.css"],
})
export class ListQuotesComponent  implements OnInit, OnDestroy {
  private selectedIndex = 1;
  private categoryItems: ValueList<string>;
  private searchKeyword:string;
  private resultList = [];
  private postID = "";

  public templateSelector = (item: any, index: number, items: any) => {
    if(index == 0)
      return item.type || "header"
    else
      return item.type || "cell";
  }

  //ngModels
  isLoading:boolean = false;
  
  startLoading(){
    this.isLoading = true;
  }
  stopLoading(){
    this.isLoading = false;
  }

  constructor(private post: HttpPostService,
    private store:StoreService,
    private utils: CommonUtils,
    private routerExtensions:RouterExtensions,
    private route: ActivatedRoute){

  }

  ngOnInit() {
      this.startLoading();
      let that = this;
      this.route.queryParams.subscribe(params => {
        var selectedItem = JSON.parse(params.selectedItem);
        if(selectedItem){
          console.log(JSON.stringify(selectedItem));
          this.post.getQuotesForJob({JOB_ID:selectedItem.ID}, (msg) => {
            console.log(JSON.stringify(msg));
            this.stopLoading();
            msg.forEach(element => {
              element.quoteDate = 'Date : ' + moment(element.post_date).format('MMM DD YYYY');
              element.quoteName = 'Name : ' + element.quoteName;
              element.quoteEmail = 'Email : ' + element.quoteEmail;
              element.quoteDocName = element.quoteDocument.substring(element.quoteDocument.lastIndexOf('/')+1);
              element.quoteComments = 'Comments : ' + element.quoteComments;
              this.resultList.push(element);
            });
          });
        }
      });
  }    

  ngOnDestroy(){}

  public onClickList(args){
    var selectedItem = this.resultList[args.index];
    utils.openUrl(selectedItem.quoteDocument);
  }


  public dummyClick(){
    this.routerExtensions.navigate(['/myJobs']);
  }

}