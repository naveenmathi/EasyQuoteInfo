"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var frameModule = require("ui/frame");
var HttpServicePost_1 = require("../../shared/service/HttpServicePost");
var StoreService_1 = require("../../shared/service/StoreService");
var CommonUtils_1 = require("../../utils/CommonUtils");
var router_1 = require("nativescript-angular/router");
var router_2 = require("@angular/router");
var moment = require("moment");
var utils = require("tns-core-modules/utils/utils");
var topmost = frameModule.topmost();
var ListQuotesComponent = /** @class */ (function () {
    function ListQuotesComponent(post, store, utils, routerExtensions, route) {
        this.post = post;
        this.store = store;
        this.utils = utils;
        this.routerExtensions = routerExtensions;
        this.route = route;
        this.selectedIndex = 1;
        this.resultList = [];
        this.postID = "";
        this.templateSelector = function (item, index, items) {
            if (index == 0)
                return item.type || "header";
            else
                return item.type || "cell";
        };
        //ngModels
        this.isLoading = false;
    }
    ListQuotesComponent.prototype.startLoading = function () {
        this.isLoading = true;
    };
    ListQuotesComponent.prototype.stopLoading = function () {
        this.isLoading = false;
    };
    ListQuotesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.startLoading();
        var that = this;
        this.route.queryParams.subscribe(function (params) {
            var selectedItem = JSON.parse(params.selectedItem);
            if (selectedItem) {
                console.log(JSON.stringify(selectedItem));
                _this.post.getQuotesForJob({ JOB_ID: selectedItem.ID }, function (msg) {
                    console.log(JSON.stringify(msg));
                    _this.stopLoading();
                    msg.forEach(function (element) {
                        element.quoteDate = 'Date : ' + moment(element.post_date).format('MMM DD YYYY');
                        element.quoteName = 'Name : ' + element.quoteName;
                        element.quoteEmail = 'Email : ' + element.quoteEmail;
                        element.quoteDocName = element.quoteDocument.substring(element.quoteDocument.lastIndexOf('/') + 1);
                        element.quoteComments = 'Comments : ' + element.quoteComments;
                        _this.resultList.push(element);
                    });
                });
            }
        });
    };
    ListQuotesComponent.prototype.ngOnDestroy = function () { };
    ListQuotesComponent.prototype.onClickList = function (args) {
        var selectedItem = this.resultList[args.index];
        utils.openUrl(selectedItem.quoteDocument);
    };
    ListQuotesComponent.prototype.dummyClick = function () {
        this.routerExtensions.navigate(['/myJobs']);
    };
    ListQuotesComponent = __decorate([
        core_1.Component({
            templateUrl: "./pages/listQuotes/listQuotes.html",
            styleUrls: ["./pages/listQuotes/listQuotes-common.css"],
        }),
        __metadata("design:paramtypes", [HttpServicePost_1.HttpPostService,
            StoreService_1.StoreService,
            CommonUtils_1.CommonUtils,
            router_1.RouterExtensions,
            router_2.ActivatedRoute])
    ], ListQuotesComponent);
    return ListQuotesComponent;
}());
exports.ListQuotesComponent = ListQuotesComponent;
