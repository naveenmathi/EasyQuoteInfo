import { Component, NgZone, OnInit, OnDestroy } from "@angular/core";
import { Page, ShownModallyData, NavigatedData } from "tns-core-modules/ui/page";
import { StoreService } from "../../shared/service/StoreService";
import { ValueList } from "nativescript-drop-down";
import { CommonUtils } from "../../utils/CommonUtils";
import { HttpPostService } from "../../shared/service/HttpServicePost";
import { Mediafilepicker, FilePickerOptions, ImagePickerOptions } from 'nativescript-mediafilepicker';
// import * as imagepicker from 'nativescript-imagepicker';
import * as app from 'application';
import {RouterExtensions} from "nativescript-angular/router";
// import {ChangeDetectorRef} from '@angular/core'

@Component({
  templateUrl: "./pages/submitQuote/submitQuote.html",
  styleUrls: ["./pages/submitQuote/submitQuote-common.css", "./pages/submitQuote/submitQuote.css"],
})

export class SubmitQuoteComponent implements OnInit, OnDestroy {

  private jobTitle:string = "";
  private jobContent:string = "";
  private jobCategory:number = -1;
  private jobFirstName:string = "";
  private jobLastName:string = "";
  private jobEmailAddress:string = "";
  private jobBusinessName:string = "";
  private jobBusinessType:number = -1;
  private jobCurrentInsurer:number = -1;
  private jobHomeInsuranceType:number = -1;
  private jobTotalValueToInsure:string = "";
  private jobDriverExperience:number = -1;
  private jobVehicleType:number = -1;
  private jobVehicleValue:string = "";
  private jobEngineSize:number = -1;
  private jobnoClaimsDiscount:number = -1;
  private jobAccident:string = "";
  private jobDriverAge:number = -1;

  categoryItems: ValueList<string>;
  businessTypeItems: ValueList<string>;
  currentInsurerItems: ValueList<string>;
  vehicleTypeItems: ValueList<string>;
  engineSizeItems: ValueList<string>;
  driverExperienceItems: ValueList<string>;
  noClaimsDiscountItems: ValueList<string>;
  driverAgeItems: ValueList<string>;
  homeInsuranceTypeItems: ValueList<string>;


  jobFile:string = "";
  isLoading:boolean = false;

  constructor(private post: HttpPostService,
    private store: StoreService,
    private routerExtensions:RouterExtensions,
    private utils: CommonUtils,
    private zone:NgZone) {
  }

  test(){
    alert(JSON.parse(this.store.categoryList));
  }

  ngOnInit(){
    this.categoryItems = new ValueList<string>();
    this.post.getCategoryList("", msg => {
      this.store.categoryList = (JSON.stringify(msg));
      JSON.parse(this.store.categoryList).map(cat => {
        this.categoryItems.push({ value: cat.id, display: this.utils.parseAmp(cat.name) });
      });
    });

    
  
    this.post.getPodsOptions("", msg => {
      this.store.podsOptions = JSON.stringify(msg).replace("\n","\\n");

      this.businessTypeItems = new ValueList<string>();
      this.businessTypeItems.push({ value: "0", display: "" });
      let idCount = 0;
      JSON.stringify(JSON.parse(this.store.podsOptions)[1].fields.business_type.options.pick_custom).split("\\n")
      .map(item => {
        idCount++;
         this.businessTypeItems.push({ value: ""+idCount, display: this.utils.parseAmp(item.toString().replace(/"/g, ''))});
       });

       this.currentInsurerItems = new ValueList<string>();
       this.currentInsurerItems.push({ value: "0", display: "" });
       idCount = 0;
       this.store.podsOptions = JSON.stringify(msg).replace("\n","\\n");
       JSON.stringify(JSON.parse(this.store.podsOptions)[1].fields.current_insurer.options.pick_custom).split("\\n")
       .map(item => {
         idCount++;
          this.currentInsurerItems.push({ value: ""+idCount, display: this.utils.parseAmp(item.toString().replace(/"/g, ''))});
        });
 
     this.vehicleTypeItems = new ValueList<string>();
     this.vehicleTypeItems.push({ value: "0", display: "" });
       idCount = 0;
       this.store.podsOptions = JSON.stringify(msg).replace("\n","\\n");
       JSON.stringify(JSON.parse(this.store.podsOptions)[1].fields.vehicle_type.options.pick_custom).split("\\n")
       .map(item => {
         idCount++;
          this.vehicleTypeItems.push({ value: ""+idCount, display: this.utils.parseAmp(item.toString().replace(/"/g, ''))});
        });
 
     this.engineSizeItems = new ValueList<string>();
     this.engineSizeItems.push({ value: "0", display: "" });
       idCount = 0;
       this.store.podsOptions = JSON.stringify(msg).replace("\n","\\n");
       JSON.stringify(JSON.parse(this.store.podsOptions)[1].fields.engine_size.options.pick_custom).split("\\n")
       .map(item => {
         idCount++;
          this.engineSizeItems.push({ value: ""+idCount, display: this.utils.parseAmp(item.toString().replace(/"/g, ''))});
        });
 
     this.driverExperienceItems = new ValueList<string>();
     this.driverExperienceItems.push({ value: "0", display: "" });
       idCount = 0;
       this.store.podsOptions = JSON.stringify(msg).replace("\n","\\n");
       JSON.stringify(JSON.parse(this.store.podsOptions)[1].fields.driver_experience.options.pick_custom).split("\\n")
       .map(item => {
         idCount++;
          this.driverExperienceItems.push({ value: ""+idCount, display: this.utils.parseAmp(item.toString().replace(/"/g, ''))});
        });
 
     this.noClaimsDiscountItems = new ValueList<string>();
     this.noClaimsDiscountItems.push({ value: "0", display: "" });
       idCount = 0;
       this.store.podsOptions = JSON.stringify(msg).replace("\n","\\n");
       JSON.stringify(JSON.parse(this.store.podsOptions)[1].fields.no_claims_discount.options.pick_custom).split("\\n")
       .map(item => {
         idCount++;
          this.noClaimsDiscountItems.push({ value: ""+idCount, display: this.utils.parseAmp(item.toString().replace(/"/g, ''))});
        });
 
        this.driverAgeItems = new ValueList<string>();
        this.driverAgeItems.push({ value: "0", display: "" });
        idCount = 0;
        this.store.podsOptions = JSON.stringify(msg).replace("\n","\\n");
        JSON.stringify(JSON.parse(this.store.podsOptions)[1].fields.driver_age.options.pick_custom).split("\\n")
        .map(item => {
          idCount++;
           this.driverAgeItems.push({ value: ""+idCount, display: this.utils.parseAmp(item.toString().replace(/"/g, ''))});
         });

         this.homeInsuranceTypeItems = new ValueList<string>();
         this.homeInsuranceTypeItems.push({ value: "0", display: "" });
         idCount = 0;
         this.store.podsOptions = JSON.stringify(msg).replace("\n","\\n");
         JSON.stringify(JSON.parse(this.store.podsOptions)[1].fields.home_insurance_type.options.pick_custom).split("\\n")
         .map(item => {
           idCount++;
            this.homeInsuranceTypeItems.push({ value: ""+idCount, display: this.utils.parseAmp(item.toString().replace(/"/g, ''))});
          });

          

    });

    

    this.jobFirstName = this.store.firstName;
    this.jobLastName = this.store.lastName;
    this.jobEmailAddress = this.store.userMail;
  }

  ngOnDestroy() {
  }

  btnPostJob() {
    console.log(this.jobCategory);
    if(this.jobCategory == -1){
      alert("Please select a category")
    }else{
      this.isLoading = true;
      var data = {
        formData:{
          post_author: this.store.userId,
          post_title: this.jobTitle,
          post_content: this.jobContent,
          job_category: this.categoryItems.getValue(this.jobCategory),
          first_name: this.jobFirstName,
          last_name: this.jobLastName,
          email_address: this.jobEmailAddress,
          business_name: this.jobBusinessName,
          business_type: this.businessTypeItems.getDisplay(this.jobBusinessType),
          current_insurer: this.currentInsurerItems.getDisplay(this.jobCurrentInsurer),
          home_insurance_type: this.homeInsuranceTypeItems.getDisplay(this.jobHomeInsuranceType),
          total_value_to_insure_bds: this.jobTotalValueToInsure,
          vehicle_type: this.vehicleTypeItems.getDisplay(this.jobVehicleType),
          vehicle_value: this.jobVehicleValue,
          engine_size: this.engineSizeItems.getDisplay(this.jobEngineSize),
          driver_experience: this.driverExperienceItems.getDisplay(this.jobDriverExperience),
          accidents_in_the_last_5_years: this.jobAccident,
          no_claims_discount: this.noClaimsDiscountItems.getDisplay(this.jobnoClaimsDiscount),
          driver_age: this.driverAgeItems.getDisplay(this.jobDriverAge),

        },
        jobFile: this.jobFile,
      }
      if(this.isValid(data.formData)){
        // console.log(JSON.stringify(data));
        this.post.postJob(data, res => {
          alert("Job posted successfully");
          this.clearData();
          this.isLoading = false;
        });
      }else{
        this.isLoading = false;
      }
    }
  }

  isValid(data):boolean{
    if(!data.job_category || !data.first_name || !data.last_name
      || !data.email_address){
          alert("Fill all required fields!");
          return false;
      }else{
        if(!this.utils.is_email(data.email_address)){
          alert("Please enter valid email address!")
          return false;
        }
      }
      return true;
  }

  clearData() {
    this.jobTitle = "";
    this.jobContent = "";
    this.jobCategory = -1;
    this.jobFile = "";
    this.jobFirstName = this.store.firstName;
    this.jobLastName = this.store.lastName;
    this.jobEmailAddress = this.store.userMail;
  }

  openFilePicker() {
    // this.isLoading = true;
    let extensions = [];
    if (app.ios) {
      extensions = ['txt', 'pdf'];
      // extensions = [kUTTypePDF, kUTTypeText]; //you can get more types from here: https://developer.apple.com/documentation/mobilecoreservices/uttype
    } else if(app.android) {
      extensions = ['jpg', 'jpeg', 'png'];
    }
    let options: FilePickerOptions = {
      android: {
        extensions: extensions,
        maxNumberFiles: 1,
      },
      ios: {
        extensions: extensions,
        multipleSelection: false
      }
    };
    let imgOptions: ImagePickerOptions = {
      android: {
        isNeedCamera: true,
        maxNumberFiles: 1,
        isNeedFolderList: true
      },
      ios: {
        isCaptureMood: false,
        maxNumberFiles: 1
      }
    };
    let mediafilepicker = new Mediafilepicker();
    // mediafilepicker.openFilePicker(options);
    mediafilepicker.openImagePicker(imgOptions);
    var that = this;
    mediafilepicker.on("getFiles", function (res) {
      this.isLoading = false;
      let results = res.object.get('results');
      that.jobFile = results[0].file;
      that.zone.run(() =>{
        that.jobFile = that.jobFile;
      });   
      console.log(results[0].file);
    });
    mediafilepicker.on("error", function (res) {
      this.isLoading = false;
      let msg = res.object.get('msg');
      console.log(msg);
    });
    mediafilepicker.on("cancel", function (res) {
      this.isLoading = false;
      let msg = res.object.get('msg');
      console.log(msg);
    });
  }
 
}