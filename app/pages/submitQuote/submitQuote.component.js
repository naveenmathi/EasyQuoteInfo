"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var StoreService_1 = require("../../shared/service/StoreService");
var nativescript_drop_down_1 = require("nativescript-drop-down");
var CommonUtils_1 = require("../../utils/CommonUtils");
var HttpServicePost_1 = require("../../shared/service/HttpServicePost");
var nativescript_mediafilepicker_1 = require("nativescript-mediafilepicker");
// import * as imagepicker from 'nativescript-imagepicker';
var app = require("application");
var router_1 = require("nativescript-angular/router");
// import {ChangeDetectorRef} from '@angular/core'
var SubmitQuoteComponent = /** @class */ (function () {
    function SubmitQuoteComponent(post, store, routerExtensions, utils, zone) {
        this.post = post;
        this.store = store;
        this.routerExtensions = routerExtensions;
        this.utils = utils;
        this.zone = zone;
        this.jobTitle = "";
        this.jobContent = "";
        this.jobCategory = -1;
        this.jobFirstName = "";
        this.jobLastName = "";
        this.jobEmailAddress = "";
        this.jobBusinessName = "";
        this.jobBusinessType = -1;
        this.jobCurrentInsurer = -1;
        this.jobHomeInsuranceType = -1;
        this.jobTotalValueToInsure = "";
        this.jobDriverExperience = -1;
        this.jobVehicleType = -1;
        this.jobVehicleValue = "";
        this.jobEngineSize = -1;
        this.jobnoClaimsDiscount = -1;
        this.jobAccident = "";
        this.jobDriverAge = -1;
        this.jobFile = "";
        this.isLoading = false;
    }
    SubmitQuoteComponent.prototype.test = function () {
        alert(JSON.parse(this.store.categoryList));
    };
    SubmitQuoteComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.categoryItems = new nativescript_drop_down_1.ValueList();
        this.post.getCategoryList("", function (msg) {
            _this.store.categoryList = (JSON.stringify(msg));
            JSON.parse(_this.store.categoryList).map(function (cat) {
                _this.categoryItems.push({ value: cat.id, display: _this.utils.parseAmp(cat.name) });
            });
        });
        this.post.getPodsOptions("", function (msg) {
            _this.store.podsOptions = JSON.stringify(msg).replace("\n", "\\n");
            _this.businessTypeItems = new nativescript_drop_down_1.ValueList();
            _this.businessTypeItems.push({ value: "0", display: "" });
            var idCount = 0;
            JSON.stringify(JSON.parse(_this.store.podsOptions)[1].fields.business_type.options.pick_custom).split("\\n")
                .map(function (item) {
                idCount++;
                _this.businessTypeItems.push({ value: "" + idCount, display: _this.utils.parseAmp(item.toString().replace(/"/g, '')) });
            });
            _this.currentInsurerItems = new nativescript_drop_down_1.ValueList();
            _this.currentInsurerItems.push({ value: "0", display: "" });
            idCount = 0;
            _this.store.podsOptions = JSON.stringify(msg).replace("\n", "\\n");
            JSON.stringify(JSON.parse(_this.store.podsOptions)[1].fields.current_insurer.options.pick_custom).split("\\n")
                .map(function (item) {
                idCount++;
                _this.currentInsurerItems.push({ value: "" + idCount, display: _this.utils.parseAmp(item.toString().replace(/"/g, '')) });
            });
            _this.vehicleTypeItems = new nativescript_drop_down_1.ValueList();
            _this.vehicleTypeItems.push({ value: "0", display: "" });
            idCount = 0;
            _this.store.podsOptions = JSON.stringify(msg).replace("\n", "\\n");
            JSON.stringify(JSON.parse(_this.store.podsOptions)[1].fields.vehicle_type.options.pick_custom).split("\\n")
                .map(function (item) {
                idCount++;
                _this.vehicleTypeItems.push({ value: "" + idCount, display: _this.utils.parseAmp(item.toString().replace(/"/g, '')) });
            });
            _this.engineSizeItems = new nativescript_drop_down_1.ValueList();
            _this.engineSizeItems.push({ value: "0", display: "" });
            idCount = 0;
            _this.store.podsOptions = JSON.stringify(msg).replace("\n", "\\n");
            JSON.stringify(JSON.parse(_this.store.podsOptions)[1].fields.engine_size.options.pick_custom).split("\\n")
                .map(function (item) {
                idCount++;
                _this.engineSizeItems.push({ value: "" + idCount, display: _this.utils.parseAmp(item.toString().replace(/"/g, '')) });
            });
            _this.driverExperienceItems = new nativescript_drop_down_1.ValueList();
            _this.driverExperienceItems.push({ value: "0", display: "" });
            idCount = 0;
            _this.store.podsOptions = JSON.stringify(msg).replace("\n", "\\n");
            JSON.stringify(JSON.parse(_this.store.podsOptions)[1].fields.driver_experience.options.pick_custom).split("\\n")
                .map(function (item) {
                idCount++;
                _this.driverExperienceItems.push({ value: "" + idCount, display: _this.utils.parseAmp(item.toString().replace(/"/g, '')) });
            });
            _this.noClaimsDiscountItems = new nativescript_drop_down_1.ValueList();
            _this.noClaimsDiscountItems.push({ value: "0", display: "" });
            idCount = 0;
            _this.store.podsOptions = JSON.stringify(msg).replace("\n", "\\n");
            JSON.stringify(JSON.parse(_this.store.podsOptions)[1].fields.no_claims_discount.options.pick_custom).split("\\n")
                .map(function (item) {
                idCount++;
                _this.noClaimsDiscountItems.push({ value: "" + idCount, display: _this.utils.parseAmp(item.toString().replace(/"/g, '')) });
            });
            _this.driverAgeItems = new nativescript_drop_down_1.ValueList();
            _this.driverAgeItems.push({ value: "0", display: "" });
            idCount = 0;
            _this.store.podsOptions = JSON.stringify(msg).replace("\n", "\\n");
            JSON.stringify(JSON.parse(_this.store.podsOptions)[1].fields.driver_age.options.pick_custom).split("\\n")
                .map(function (item) {
                idCount++;
                _this.driverAgeItems.push({ value: "" + idCount, display: _this.utils.parseAmp(item.toString().replace(/"/g, '')) });
            });
            _this.homeInsuranceTypeItems = new nativescript_drop_down_1.ValueList();
            _this.homeInsuranceTypeItems.push({ value: "0", display: "" });
            idCount = 0;
            _this.store.podsOptions = JSON.stringify(msg).replace("\n", "\\n");
            JSON.stringify(JSON.parse(_this.store.podsOptions)[1].fields.home_insurance_type.options.pick_custom).split("\\n")
                .map(function (item) {
                idCount++;
                _this.homeInsuranceTypeItems.push({ value: "" + idCount, display: _this.utils.parseAmp(item.toString().replace(/"/g, '')) });
            });
        });
        this.jobFirstName = this.store.firstName;
        this.jobLastName = this.store.lastName;
        this.jobEmailAddress = this.store.userMail;
    };
    SubmitQuoteComponent.prototype.ngOnDestroy = function () {
    };
    SubmitQuoteComponent.prototype.btnPostJob = function () {
        var _this = this;
        console.log(this.jobCategory);
        if (this.jobCategory == -1) {
            alert("Please select a category");
        }
        else {
            this.isLoading = true;
            var data = {
                formData: {
                    post_author: this.store.userId,
                    post_title: this.jobTitle,
                    post_content: this.jobContent,
                    job_category: this.categoryItems.getValue(this.jobCategory),
                    first_name: this.jobFirstName,
                    last_name: this.jobLastName,
                    email_address: this.jobEmailAddress,
                    business_name: this.jobBusinessName,
                    business_type: this.businessTypeItems.getDisplay(this.jobBusinessType),
                    current_insurer: this.currentInsurerItems.getDisplay(this.jobCurrentInsurer),
                    home_insurance_type: this.homeInsuranceTypeItems.getDisplay(this.jobHomeInsuranceType),
                    total_value_to_insure_bds: this.jobTotalValueToInsure,
                    vehicle_type: this.vehicleTypeItems.getDisplay(this.jobVehicleType),
                    vehicle_value: this.jobVehicleValue,
                    engine_size: this.engineSizeItems.getDisplay(this.jobEngineSize),
                    driver_experience: this.driverExperienceItems.getDisplay(this.jobDriverExperience),
                    accidents_in_the_last_5_years: this.jobAccident,
                    no_claims_discount: this.noClaimsDiscountItems.getDisplay(this.jobnoClaimsDiscount),
                    driver_age: this.driverAgeItems.getDisplay(this.jobDriverAge),
                },
                jobFile: this.jobFile,
            };
            if (this.isValid(data.formData)) {
                // console.log(JSON.stringify(data));
                this.post.postJob(data, function (res) {
                    alert("Job posted successfully");
                    _this.clearData();
                    _this.isLoading = false;
                });
            }
            else {
                this.isLoading = false;
            }
        }
    };
    SubmitQuoteComponent.prototype.isValid = function (data) {
        if (!data.job_category || !data.first_name || !data.last_name
            || !data.email_address) {
            alert("Fill all required fields!");
            return false;
        }
        else {
            if (!this.utils.is_email(data.email_address)) {
                alert("Please enter valid email address!");
                return false;
            }
        }
        return true;
    };
    SubmitQuoteComponent.prototype.clearData = function () {
        this.jobTitle = "";
        this.jobContent = "";
        this.jobCategory = -1;
        this.jobFile = "";
        this.jobFirstName = this.store.firstName;
        this.jobLastName = this.store.lastName;
        this.jobEmailAddress = this.store.userMail;
    };
    SubmitQuoteComponent.prototype.openFilePicker = function () {
        // this.isLoading = true;
        var extensions = [];
        if (app.ios) {
            extensions = ['txt', 'pdf'];
            // extensions = [kUTTypePDF, kUTTypeText]; //you can get more types from here: https://developer.apple.com/documentation/mobilecoreservices/uttype
        }
        else if (app.android) {
            extensions = ['jpg', 'jpeg', 'png'];
        }
        var options = {
            android: {
                extensions: extensions,
                maxNumberFiles: 1,
            },
            ios: {
                extensions: extensions,
                multipleSelection: false
            }
        };
        var imgOptions = {
            android: {
                isNeedCamera: true,
                maxNumberFiles: 1,
                isNeedFolderList: true
            },
            ios: {
                isCaptureMood: false,
                maxNumberFiles: 1
            }
        };
        var mediafilepicker = new nativescript_mediafilepicker_1.Mediafilepicker();
        // mediafilepicker.openFilePicker(options);
        mediafilepicker.openImagePicker(imgOptions);
        var that = this;
        mediafilepicker.on("getFiles", function (res) {
            this.isLoading = false;
            var results = res.object.get('results');
            that.jobFile = results[0].file;
            that.zone.run(function () {
                that.jobFile = that.jobFile;
            });
            console.log(results[0].file);
        });
        mediafilepicker.on("error", function (res) {
            this.isLoading = false;
            var msg = res.object.get('msg');
            console.log(msg);
        });
        mediafilepicker.on("cancel", function (res) {
            this.isLoading = false;
            var msg = res.object.get('msg');
            console.log(msg);
        });
    };
    SubmitQuoteComponent = __decorate([
        core_1.Component({
            templateUrl: "./pages/submitQuote/submitQuote.html",
            styleUrls: ["./pages/submitQuote/submitQuote-common.css", "./pages/submitQuote/submitQuote.css"],
        }),
        __metadata("design:paramtypes", [HttpServicePost_1.HttpPostService,
            StoreService_1.StoreService,
            router_1.RouterExtensions,
            CommonUtils_1.CommonUtils,
            core_1.NgZone])
    ], SubmitQuoteComponent);
    return SubmitQuoteComponent;
}());
exports.SubmitQuoteComponent = SubmitQuoteComponent;
