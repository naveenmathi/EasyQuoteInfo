"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var frameModule = require("ui/frame");
var HttpServicePost_1 = require("../../shared/service/HttpServicePost");
var StoreService_1 = require("../../shared/service/StoreService");
var nativescript_drop_down_1 = require("nativescript-drop-down");
var CommonUtils_1 = require("../../utils/CommonUtils");
var core_2 = require("@angular/core");
var topmost = frameModule.topmost();
var HomeComponent = /** @class */ (function () {
    function HomeComponent(post, store, utils, cd, zone) {
        this.post = post;
        this.store = store;
        this.utils = utils;
        this.cd = cd;
        this.zone = zone;
        this.selectedIndex = 1;
        this.searchString = "";
        this.resultList = [];
        this.selectedCategory = 0;
        this.selectedCountry = 0;
        this.selectedCity = 0;
        this.selectedItem = {};
        this.isLoading = false;
        this.isDetailed = false;
        this.isInitialized = false;
        this.templateSelector = function (item, index, items) {
            if (index == 0)
                return item.type || "header";
            else
                return item.type || "cell";
        };
    }
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.resultList.push({});
        this.isLoading = true;
        console.log("init of home");
        this.categoryItems = new nativescript_drop_down_1.ValueList();
        this.categoryItems.push({ value: "", display: "Select Category" });
        this.post.getCategoryList("", function (msg) {
            _this.store.categoryList = JSON.stringify(msg);
            JSON.parse(_this.store.categoryList).map(function (cat) {
                _this.categoryItems.push({ value: cat.id, display: _this.utils.parseAmp(cat.name) });
            });
            _this.isLoading = false;
        });
        //initialize countries and cities
        this.countryItems = new nativescript_drop_down_1.ValueList();
        this.countryItems.push({ value: JSON.stringify({}), display: "Select Country" });
        this.cityItems = new nativescript_drop_down_1.ValueList();
        this.cityItems.push({ value: "", display: "Select City" });
        this.post.getCoutries("", function (msg) {
            console.log("\n  Countires  ............... \n");
            console.log(JSON.stringify(msg));
            var c2 = [{ name: "", cities: [] }];
            msg.forEach(function (msgVal) {
                var found = false;
                for (var i = 0; i < c2.length; i++) {
                    if (c2[i].name == msgVal.country) {
                        c2[i].cities.push(msgVal.city);
                        found = true;
                        break;
                    }
                }
                if (!found)
                    c2.push({ name: msgVal.country, cities: [msgVal.city] });
            });
            c2.forEach(function (element) {
                _this.countryItems.push({ value: JSON.stringify(element), display: element.name });
            });
        });
        this.isInitialized = true;
    };
    HomeComponent.prototype.ngOnDestroy = function () { };
    HomeComponent.prototype.onchangeCountry = function (args) {
        var _this = this;
        this.cityItems = new nativescript_drop_down_1.ValueList();
        this.cityItems.push({ value: "", display: "Select City" });
        this.selectedCity = 0;
        if (this.countryItems.getValue(args.newIndex) != JSON.stringify({})) {
            var country = JSON.parse(this.countryItems.getValue(args.newIndex));
            country.cities.map(function (city) {
                _this.cityItems.push({ value: city, display: city });
                // console.log(city)
            });
        }
        // this.zone.run(()=>{
        //   this.cityItems = this.cityItems;
        //   this.cd.detectChanges();
        // });
    };
    HomeComponent.prototype.btnFindProvider = function () {
        var _this = this;
        this.isLoading = true;
        var country = JSON.parse(this.countryItems.getValue(this.selectedCountry));
        var city = this.cityItems.getValue(this.selectedCity);
        var data = { searchString: this.validate(this.searchString), categoryId: this.categoryItems.getValue(this.selectedCategory),
            country: this.validate((country.name ? country.name : "")), city: this.validate(city) };
        //  console.log("Find button clicked: "+JSON.stringify(data));
        this.post.findProvider(data, function (msg) {
            _this.isLoading = false;
            // console.log(JSON.stringify(msg));
            // console.log(msg.length);
            _this.resultList = [{}];
            if (msg.length > 0) {
                _this.resultList = _this.resultList.concat(msg);
            }
            else {
                alert("No Provider Found!");
            }
            _this.isInitialized = true;
        });
    };
    HomeComponent.prototype.validate = function (str) {
        if (str && str.length > 0) {
            return str.replace(' ', '+');
        }
        return "";
    };
    HomeComponent.prototype.dummyClick = function () {
        this.isDetailed = false;
        this.selectedItem = {};
    };
    HomeComponent.prototype.showDetail = function (arg) {
        // console.log(JSON.stringify(arg));
        this.selectedItem = arg;
        this.isDetailed = true;
    };
    HomeComponent.prototype.getCategoryNames = function () {
        var _this = this;
        var output = "";
        var cats = this.selectedItem.category_id;
        if (cats) {
            cats.split(',').map(function (item) {
                output = output + ', ' + _this.getCategoryName(item);
            });
        }
        else {
            output = " ";
        }
        return output.substring(1, output.length).replace('/&amp;/g', '&');
    };
    HomeComponent.prototype.getCategoryName = function (catId) {
        var catList = [];
        catList = JSON.parse(this.store.categoryList);
        return catList.find(function (o) { return o.id == catId; }).name;
    };
    HomeComponent = __decorate([
        core_1.Component({
            templateUrl: "./pages/home/home.html",
            styleUrls: ["./pages/home/home-common.css", "./pages/home/home.css"],
        }),
        __metadata("design:paramtypes", [HttpServicePost_1.HttpPostService,
            StoreService_1.StoreService,
            CommonUtils_1.CommonUtils,
            core_2.ChangeDetectorRef,
            core_1.NgZone])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
