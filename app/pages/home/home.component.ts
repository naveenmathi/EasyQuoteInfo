import { Component, OnInit, NgZone, OnDestroy } from "@angular/core";
import * as frameModule from "ui/frame";
import { SelectedIndexChangedEventData } from "nativescript-drop-down";
import { HttpPostService } from "../../shared/service/HttpServicePost";
import { StoreService } from "../../shared/service/StoreService";
import { ValueList } from "nativescript-drop-down";
import view = require("ui/core/view");
import { CommonUtils } from "../../utils/CommonUtils";
import {ChangeDetectorRef} from '@angular/core'

const topmost = frameModule.topmost();

@Component({
  templateUrl:"./pages/home/home.html",
  styleUrls:["./pages/home/home-common.css", "./pages/home/home.css"],
})
export class HomeComponent implements OnInit,OnDestroy {
  private selectedIndex = 1;
  private categoryItems: ValueList<string>;
  private countryItems: ValueList<string>;
  private cityItems: ValueList<string>;
  private searchString:string  = "";
  private resultList = [];
  private selectedCategory:number = 0;
  private selectedCountry:number  = 0;
  private selectedCity:number  = 0;
  private selectedItem:any = {};
  isLoading:boolean = false;
  isDetailed:boolean = false;
  isInitialized:boolean = false;

  public templateSelector = (item: any, index: number, items: any) => {
    if(index == 0)
      return item.type || "header"
    else
      return item.type || "cell";
  }

  constructor(private post: HttpPostService,
    private store:StoreService,
    private utils: CommonUtils,
    private cd: ChangeDetectorRef,
    private zone: NgZone){
  }

  ngOnInit(){
    this.resultList.push({});
    this.isLoading = true;
    console.log("init of home");
    this.categoryItems = new ValueList<string>();
    this.categoryItems.push({value:"",display:"Select Category"});
    this.post.getCategoryList("", msg => {
      this.store.categoryList = JSON.stringify(msg);
      JSON.parse(this.store.categoryList).map(cat => {
        this.categoryItems.push({ value: cat.id, display: this.utils.parseAmp(cat.name) });
      });
      this.isLoading = false;
    });
   
    //initialize countries and cities
    this.countryItems = new ValueList<string>();
    this.countryItems.push({value:JSON.stringify({}), display:"Select Country"});
    this.cityItems = new ValueList<string>();
    this.cityItems.push({value:"",display:"Select City"});
    this.post.getCoutries("", msg => {
      console.log("\n  Countires  ............... \n");
      console.log(JSON.stringify(msg));
      let c2 = [{name:"",cities:[]}];
      msg.forEach(msgVal => {
        let found = false;
        for(let i=0; i<c2.length; i++){
          if(c2[i].name == msgVal.country){
            c2[i].cities.push(msgVal.city);
            found = true;
            break;
          }
        }
        if(!found)
          c2.push({name:msgVal.country,cities:[msgVal.city]});
      });
      
      c2.forEach(element => {
        this.countryItems.push({ value: JSON.stringify(element), display: element.name});
      });
    });
   
    this.isInitialized = true;
  }

  ngOnDestroy(){}

  onchangeCountry(args: SelectedIndexChangedEventData){
    this.cityItems = new ValueList<string>();
    this.cityItems.push({value:"",display:"Select City"});
    this.selectedCity = 0;
    if(this.countryItems.getValue(args.newIndex) != JSON.stringify({})){
      let country = JSON.parse(this.countryItems.getValue(args.newIndex));
      country.cities.map(city => {
        this.cityItems.push({ value: city, display: city});
        // console.log(city)
      });
    }
    // this.zone.run(()=>{
    //   this.cityItems = this.cityItems;
    //   this.cd.detectChanges();
    // });
  }

  btnFindProvider(){
    this.isLoading = true;
    let country = JSON.parse(this.countryItems.getValue(this.selectedCountry));
    let city = this.cityItems.getValue(this.selectedCity);
    var data = {searchString:this.validate(this.searchString), categoryId:this.categoryItems.getValue(this.selectedCategory),
       country:this.validate((country.name?country.name:"")), city:this.validate(city)};
      //  console.log("Find button clicked: "+JSON.stringify(data));
    this.post.findProvider(data,(msg) => {
      this.isLoading = false;
      // console.log(JSON.stringify(msg));
      // console.log(msg.length);
      this.resultList=[{}];
      if(msg.length > 0){
        this.resultList=this.resultList.concat(msg);
      }else{
        alert("No Provider Found!")
      }
      this.isInitialized = true;
    });

  }

  validate(str:String){
    if(str && str.length>0){
      return str.replace(' ','+');
    }
    return "";
  }

  dummyClick(){
    this.isDetailed = false;
    this.selectedItem = {};
  }

  showDetail(arg){
    // console.log(JSON.stringify(arg));
    this.selectedItem = arg;
    this.isDetailed = true;
  }

  getCategoryNames(){
    let output:string = "";
    let cats:string = this.selectedItem.category_id;
    if(cats){
      cats.split(',').map(item => {
        output = output + ', ' + this.getCategoryName(item);
      })
    }else{
      output = " ";
    }
    return output.substring(1,output.length).replace('/&amp;/g','&');
  }

  getCategoryName(catId:string) :string{
    let catList = [];
    catList = JSON.parse(this.store.categoryList);
    return catList.find(o => o.id == catId).name;
  }
}