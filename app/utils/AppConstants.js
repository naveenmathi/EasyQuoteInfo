"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AppConstants = /** @class */ (function () {
    function AppConstants() {
    }
    AppConstants.RESOURCES_PATH = new String("../images");
    return AppConstants;
}());
exports.AppConstants = AppConstants;
