import {Injectable} from "@angular/core";

@Injectable()
export class CommonUtils{

    parseAmp(encodedStr:string) :string{
        return encodedStr.replace(new RegExp('&amp;', 'g'), '&');
    }
    is_email(email) :boolean{      
        var emailReg = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        return emailReg.test(email); 
    } 

    getCoutries() :Array<any>{
        let countries=[
            {
                name:"Barbados",
                cities:["Bridgetown","Carrington","Durants","Graveyard","Mile and A Quarter","ST.Martins","Saint Marks","St Marks"]
            },
            {
                name:"India",
                cities:["Chennai"]
            }
        ];
        return countries;
    }

}