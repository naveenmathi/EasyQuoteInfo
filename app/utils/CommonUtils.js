"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var CommonUtils = /** @class */ (function () {
    function CommonUtils() {
    }
    CommonUtils.prototype.parseAmp = function (encodedStr) {
        return encodedStr.replace(new RegExp('&amp;', 'g'), '&');
    };
    CommonUtils.prototype.is_email = function (email) {
        var emailReg = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        return emailReg.test(email);
    };
    CommonUtils.prototype.getCoutries = function () {
        var countries = [
            {
                name: "Barbados",
                cities: ["Bridgetown", "Carrington", "Durants", "Graveyard", "Mile and A Quarter", "ST.Martins", "Saint Marks", "St Marks"]
            },
            {
                name: "India",
                cities: ["Chennai"]
            }
        ];
        return countries;
    };
    CommonUtils = __decorate([
        core_1.Injectable()
    ], CommonUtils);
    return CommonUtils;
}());
exports.CommonUtils = CommonUtils;
