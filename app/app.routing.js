"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var login_component_1 = require("./pages/login/login.component");
var submitQuote_component_1 = require("./pages/submitQuote/submitQuote.component");
var home_component_1 = require("./pages/home/home.component");
var signup_component_1 = require("./pages/signup/signup.component");
var jobAlerts_component_1 = require("./pages/jobAlerts/jobAlerts.component");
var myJobs_component_1 = require("./pages/myJobs/myJobs.component");
var submitVendorQ_component_1 = require("./pages/submitVendorQ/submitVendorQ.component");
var listQuotes_component_1 = require("./pages/listQuotes/listQuotes.component");
exports.routes = [
    { path: "home", component: home_component_1.HomeComponent },
    { path: "login", component: login_component_1.LoginComponent },
    { path: "signup", component: signup_component_1.SignupComponent },
    { path: "submitQuote", component: submitQuote_component_1.SubmitQuoteComponent },
    { path: "jobAlerts", component: jobAlerts_component_1.JobAlertsComponent },
    { path: "myJobs", component: myJobs_component_1.MyJobsComponent },
    { path: "submitVendorQ", component: submitVendorQ_component_1.SubmitVendorQComponent },
    { path: "listQuotes", component: listQuotes_component_1.ListQuotesComponent },
];
var componentBuilder = [];
for (var _i = 0, routes_1 = exports.routes; _i < routes_1.length; _i++) {
    var route = routes_1[_i];
    componentBuilder.push(route.component);
}
exports.navigatableComponents = componentBuilder;
